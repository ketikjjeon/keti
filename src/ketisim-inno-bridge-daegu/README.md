# KETI Simulator ROS bridge - inno simulation version

This project is ROS bridge system which convert protocol from the inno's simulator to the ROS KETI messages

current protocol version: v0.4.2

## Dependency

  - protobuf : v3.12.3

  - Open Simulator Interface : v3.2.0
  
  - libboost : 1.58.0+  (!! Do not use v1.69 ~ 1.71 !!)
    - asio
    - system
  
  - fmt : 6.2.1
  
  - blaze : v3.7+
    - libopenblas
      - libopenblas-base : 0.2.18+
      - libopenblas-dev : 0.2.18+
    
    - liblapack3
      - liblapack3 : 3.6.0+
      - liblapack-dev : 3.6.0+

  - ROS : kinetic
    - roscpp
    - tf
    - sensor_msgs
    - geometry_msgs
    - nav_msgs
    - KETI message set
      - control_msgs
      - keti_msgs

## Build & Installation

GCC version should be greater then 5.3.0!!!

First of all, install dependencies.

### Install dependencies

```sh
$ sudo apt update
$ sudo apt install libboost-all-dev libopenblas-base libopenblas-dev liblapack3 liblapack-dev autoconf automake libtool curl make g++ unzip
```

### Dependencies build

And then, build the fmt, blaze, flatbuffers


```sh
$ git submodule update --init --recursive
```

#### fmt

```sh
$ cd modules/fmt
$ mkdir build && cd build
$ cmake ..
$ make -j -l && sudo make install

```

#### protobuf

```sh
$ cd modules/protobuf
$ ./autogen.sh
$ ./configure --prefix=/usr/local
$ make -j -l && make check
$ sudo make install
$ sudo ldconfig
```

#### open simulator interface

```sh

$ cd modules/open-simulation-interface
$ sed -i '4i\set(CMAKE_CXX_STANDARD 14)' CMakeLists.txt
$ sed -i '5i\set(CMAKE_CXX_STANDARD_REQUIRED ON)' CMakeLists.txt
$ sed -i '6i\set(CMAKE_CXX_EXTENSIONS OFF)' CMakeLists.txt
$ mkdir build && cd build
$ cmake -DFILTER_PROTO2CPP_PY_PATH=../../proto2cpp ..
$ make -j -l
$ sudo make install
```

#### blaze

```sh
$ cd modules/blaze
$ cmake -DCMAKE_INSTALL_PREFIX=/usr/local/
$ sudo make install
```

### Project build

make directory to build sources.

```sh
$ cd "ROS workspace root directory"
$ catkin_make
```

## Development environment

### develope & target environment

  - Arch linux
    - kernel: 5.6.5

  - boost asio: 1.72.0-1
  
  - fmt: 6.2.1
  
  - Docker - Ubuntu 16.04
    - ros-kinetic-desktop-full

    - boost asio: 1.58

### target environment

  - Ubuntu 16.04
  
  - ros-kinetic-desktop-full

  - installed keti_auto
  
  - boost asio: 1.58

  - fmt: 6.2.1
