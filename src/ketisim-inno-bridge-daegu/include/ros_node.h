/**
 * @file ros_node.h
 *
 * @brief manage the ros datas
 *
 * @author Sangwook Ha  swid1@keti.re.kr
 *
 **/

#ifndef ROS_NODE_H_
#define ROS_NODE_H_

#include <thread>
#include <functional>

#include "osi_ros.h"
#include "network_client.h"

//! @brief OSI to ROS converter classes manager.
//!  classificate received data and process data
class RosManager : public TCPClient{
  public:
    RosManager(int argc, char** argv, io_service_sptr io_serv, RosNodeHandleSptr sptr_nh, std::string ip, int port, bool is_receive_sensor);
    ~RosManager();
    
    void connect();
    void run();

  private:
    std::string simulator_ip_;
    int simulator_port_ = 0;

    RosNodeHandleSptr sptr_nh_;

    osiNros::TopicInfoArr topic_setting_;
    std::unique_ptr<OsiRosControl> control_convert_;
    std::unique_ptr<OsiRosVehicle> state_convert_;
    std::unique_ptr<OsiRosTimer> sim_time_convert_;
    std::unique_ptr<OsiRosGT> gt_converter_;

    uint64_t camera_num_ = 0;
    uint64_t lidar_num_ = 0;
    std::unique_ptr<OsiRosCamera> sensor_camera_convert_;
    std::unique_ptr<OsiRosLidar> sensor_lidar_convert_;
    std::unique_ptr<OsiRosRadar> sensor_radar_convert_;

    SimulationVehicleDataType sim_data_type_;
    SimulationWeatherState sim_weather_;
    SensorConnectionInfo sim_sensor_info_;

    double gps_offset_x_ = 0.0;
    double gps_offset_y_ = 0.0;
    uint8_t data_flag_ = 0;
    bool server_conn_chk_;
    bool use_sim_time_;
    bool use_compressed_img_;
    bool use_gt_data_;
    bool simulation_running_ = false;

    void receiveCallback();
    size_t headerChecker();

    bool readSensorConfig();

};

#endif
