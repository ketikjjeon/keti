/**
 * @file keti_interface.h
 *
 * @brief keti interface protocol v0.4 code for communication
 *
 * @author Sangwook Ha
 * Contact: swid1@keti.re.kr
 *
 */

#ifndef KETI_INTERFACE_H_
#define KETI_INTERFACE_H_
#include <array>
#include <vector>
#include <string>
#include <fmt/core.h>


typedef std::array<unsigned int,7> SimInfoTable;

//! @brief serialize data to little endian 1byte array and push back to the buffer
//!
//! @tparam T
//! @param data
//! @param target
//!
//! @return size of the serialized data
template<typename T>
static inline size_t serialize(T data, std::vector<char> &target){
  if(sizeof(data) < 2)
    target.push_back(data);
  else{
    char* tmp_data = reinterpret_cast<char*>(&data);
    std::copy(tmp_data,tmp_data+sizeof(data),std::back_inserter(target));
  }

  return sizeof(data);
}

//! @brief deserialize data to specific type
//!
//! @tparam T
//! @param target
//!
//! @return T type value
template<typename T>
static inline T deserialize(std::vector<char>::iterator target){
  T tmp = *reinterpret_cast<T*>(target.base());
  return tmp;
}

//! @brief apply the packet body size to header
//!
//! @param data_size
//! @param target
static inline void serialSizeApply(size_t data_size, std::vector<char> &target){
  char* size_array = reinterpret_cast<char*>(&data_size);
  for(size_t i=0;i<sizeof(uint64_t);++i)
    target[4+i] = size_array[i];
}

//! @brief apply each data structures start address to data frame table
//!
//! @param table
//! @param target
static inline void serialTableSize(SimInfoTable table, std::vector<char> &target){
  for(size_t i=0;i<table.size();++i)
  {
    char *tmp = reinterpret_cast<char*>((table.begin()+i));
    target.push_back(i+1);

    for(size_t j=0;j<sizeof(unsigned int);++j)
      target.push_back(tmp[j]);
  }
}
//! @brief KETI simulator protocol data classification flag
enum DataClassFlag: unsigned char{
  FLAG_ERROR_UNKNOWN = 0,
  FLAG_SERVER_INFO = 1,
  FLAG_CLIENT_INFO = 2,
  FLAG_TIME_TICK = 3,
  FLAG_EMULATED_DATA = 4,
  FLAG_VEHICLE_STATUS = 5,
  FLAG_CONTROL_INPUT = 6,
};

//! @brief simulator connection and simulation data classification flag
enum SimulationInformation: unsigned char{
  INFO_SIMULATION_RUNNING = 0,
  INFO_SIMULATION_VEHICLE_DATA_TYPE,
  INFO_SIMULATION_WEATHER_STATE,
  INFO_SIMULATION_TIME_TICK,
  INFO_EMULATED_SENSOR_CONNECTION,
  INFO_SIMULATION_VEHICLE_STATUS,
  INFO_BRIDGE_CONNECTION_INFO,
}; 

//! @brief simulator running state flag
enum SimulationRunningState: unsigned char{
  RUNNING_NONE_EXCEPTION = 0,
  RUNNING_STOP,
  RUNNING_PAUSE,
  RUNNING_RUN,
};

//! @brief ego vehicle acceleration control input type
enum ControlInputType: unsigned char{
  INPUT_NONE_EXCEPTION = 0,
  INPUT_THROTTLE,
  INPUT_ACCELERATION,
};

//! @brief virtual gps data output type
enum GpsType: unsigned char{
  GPS_NONE_EXCEPTION = 0,
  GPS_WGS,
  GPS_UTM,
}; 

//! @brief simulated weather condition flag
enum SimulationWeatherFlag: unsigned char{
  WEATHER_NONE_EXCEPTION = 0,
  WEATHER_CLEAR,
  WEATHER_CLOUD,
  WEATHER_FOG,
  WEATHER_RAIN,
  WEATHER_SNOW,
  WEATHER_UNKNOWN,
};

//! @brief received emulated sensor data classification flag
enum SensorType: int{
  SENSOR_NONE = 0,
  SENSOR_CAMERA,
  SENSOR_LIDAR,
  SENSOR_GT,
  SENSOR_RADAR,
  SENSOR_GPSIMU,
  SENSOR_ULTRASONIC,
  SENSOR_UNKNOWN,
};

//! @brief ego vehicle brake state
enum BrakeState: unsigned char{
  BRAKE_NONE = 0,
  BRAKE_ON,
};

//! @brief ego vehicle driving mode state
enum DrivingMode: unsigned char{
  DRIVING_NONE_EXCEPTION = 0,
  DRIVING_MANUAL,
  DRIVING_FULL_AUTO,
  DRIVING_STEER_AUTO,
  DRIVING_CC_AUTO,
};

//! @brief ego vehicle gear state
enum GearState: unsigned char{
  GEAR_NONE_EXCEPTION = 0,
  GEAR_PARKING,
  GEAR_REVERSE,
  GEAR_NEUTRAL,
  GEAR_DRIVING,
};

//! @brief ego vehicle blinker state
enum BlinkerState: unsigned char{
  BLINKER_NONE_EXCEPTION = 0,
  BLINKER_LEFT,
  BLINKER_RIGHT,
  BLINKER_FLASHER,
};

//! @brief simulator input and output data type
typedef struct SimulationVehicleDataType{
  ControlInputType control_input;
  GpsType gps_type;

  //! @brief serialize vehicle data types and push back to the target buffer
  //!
  //! @param target
  //!
  //! @return total size of the vehicle data type structure
  size_t serialize(std::vector<char> &target){
    ::serialize<ControlInputType>(control_input,target);
    ::serialize<GpsType>(gps_type,target);

    return sizeof(control_input)+sizeof(gps_type);
  }

  //! @brief deserialize vehicle data types and store
  //!
  //! @param target
  void deserialize(std::vector<char>::iterator target){
    control_input = ::deserialize<ControlInputType>(target);
    gps_type = ::deserialize<GpsType>(target+sizeof(control_input));
  }
} SimulationVehicleDataType;

//! @brief simulated weather state and level
typedef struct SimulationWeatherState{
  SimulationWeatherFlag weather_state;
  unsigned char weather_level;

  //! @brief serialize simulation weather datas and push back to the target buffer 
  //!
  //! @param target
  //! 
  //! @return total size of weather datas
  size_t serialize(std::vector<char> &target){
    ::serialize<SimulationWeatherFlag>(weather_state,target);
    target.push_back(static_cast<char>(weather_level));

    return sizeof(weather_state)+sizeof(weather_level);
  }

  //! @brief deserialize simulation weather datas and store
  //!
  //! @param target
  //!
  //! @return total size of weather datas
  size_t deserialize(std::vector<char>::iterator target){
    weather_state = SimulationWeatherFlag(*reinterpret_cast<unsigned char*>(target.base()));
    weather_level = *reinterpret_cast<unsigned char*>((target+sizeof(weather_state)).base());
    return sizeof(weather_state)+sizeof(weather_level);
  }
} SimulationWeatherState;

//! @brief simulator time tick
//!  this datas mean the times which passed from start the simulator
typedef struct SimulatorTimeTick{
  unsigned long long time_sec = 0;
  unsigned long long time_nsec = 0;
  
  //! @brief display the stored time tick data
  void display(){
    fmt::print("time\tsec: {}\tnsec: {}\n",time_sec,time_nsec);
  }

  //! @brief serialize time tick data and push back to the target buffer
  //!
  //! @param target
  //!
  //! @return total size of time datas
  size_t serialize(std::vector<char> &target){
    ::serialize<unsigned long long>(time_sec,target);
    ::serialize<unsigned long long>(time_nsec,target);

    return sizeof(time_sec)+sizeof(time_nsec);
  }

  //! @brief deserialize time tick datas and store
  //!
  //! @param target
  //!
  //! @return total size of time datas
  size_t deserialize(std::vector<char>::iterator target){
    time_sec = *reinterpret_cast<unsigned long long*>(target.base());
    time_nsec = *reinterpret_cast<unsigned long long*>((target+sizeof(time_sec)).base());
    
    return sizeof(time_sec)+sizeof(time_nsec);
  }
} SimulatorTimeTick;

//! @brief simulator connection information to connect sensor emulator directly or media server
//!  not used now
typedef struct SensorConnectionInfo{
  std::string sensor_name = "";
  std::string sensor_ip = "";
  unsigned int sensor_port = 0;
  SensorType sensor_type = SensorType::SENSOR_NONE;
  std::string sensor_desc = "";
  
  //! @brief default constructor
  SensorConnectionInfo(){}

  //! @brief move constructor
  //!
  //! @param t
  SensorConnectionInfo(SensorConnectionInfo &&t) :
    sensor_name(std::move(t.sensor_name)),
    sensor_ip(std::move(t.sensor_ip)),
    sensor_port(t.sensor_port),
    sensor_type(t.sensor_type),
    sensor_desc(std::move(t.sensor_desc)){}

  //! @brief copy constructor
  //!
  //! @param t
  SensorConnectionInfo(SensorConnectionInfo &t) :
    sensor_name(t.sensor_name),
    sensor_ip(t.sensor_ip),
    sensor_port(t.sensor_port),
    sensor_type(t.sensor_type),
    sensor_desc(t.sensor_desc){}

  //! @brief const copy constructor
  //!
  //! @param t
  SensorConnectionInfo(const SensorConnectionInfo &t) :
    sensor_name(t.sensor_name),
    sensor_ip(t.sensor_ip),
    sensor_port(t.sensor_port),
    sensor_type(t.sensor_type),
    sensor_desc(t.sensor_desc){}

  //! @brief default destructor
  ~SensorConnectionInfo(){
    sensor_name.clear();
    sensor_ip.clear();
    sensor_desc.clear();
  }

  //! @brief display the stored sensor connection information
  void display(){
    fmt::print("sensor name: {}\nip: {}\tport: {}\ntype: {}\ndesc: {}\n",sensor_name, sensor_ip, sensor_port,
        sensor_type, sensor_desc);
  }

  //! @brief serialize the sensor connection datas and push back
  //!
  //! @param target
  //!
  //! @return total size of sensor connection datas
  size_t serialize(std::vector<char> &target){
    ::serialize<unsigned int>(sensor_name.size(), target);
    if(sensor_name.size()>0)
      std::copy(sensor_name.begin(),sensor_name.end(),std::back_inserter(target));
    ::serialize<unsigned int>(sensor_ip.size(), target);
    if(sensor_ip.size()>0)
      std::copy(sensor_ip.begin(),sensor_ip.end(),std::back_inserter(target));
    ::serialize<unsigned int>(sensor_port, target);
    ::serialize<SensorType>(sensor_type, target);
    ::serialize<unsigned int>(sensor_desc.size(), target);
    if(sensor_desc.size()>0)
      std::copy(sensor_desc.begin(),sensor_desc.end(),std::back_inserter(target));

    return target.size();
  }

  //! @brief deserialize the sensor connection datas and store
  //!
  //! @param target
  //!
  //! @return total size of sensor connection datas 
  size_t deserialize(std::vector<char>::iterator target){
    size_t data_idxer = 0;
    unsigned int tmp_size = *reinterpret_cast<unsigned int*>(target.base());
    sensor_name = std::string(target,target+tmp_size);
    data_idxer += sizeof(tmp_size)+tmp_size;
    tmp_size = *reinterpret_cast<unsigned int*>((target+data_idxer).base());
    data_idxer += sizeof(tmp_size);
    sensor_ip = std::string((target+data_idxer), (target+data_idxer+tmp_size));
    data_idxer += tmp_size;
    sensor_port = *reinterpret_cast<unsigned int*>((target+data_idxer).base());
    data_idxer += sizeof(sensor_port);
    sensor_type = *reinterpret_cast<SensorType*>((target+data_idxer).base());
    data_idxer += sizeof(SensorType);
    tmp_size = *reinterpret_cast<unsigned int*>((target+data_idxer).base());
    data_idxer += sizeof(tmp_size);
    sensor_desc = std::string((target+data_idxer), (target+data_idxer+tmp_size));
    data_idxer += tmp_size;

    return data_idxer;
  }
} SensorConnectionInfo;

//! @brief simulated ego vehicle position
typedef struct GpsData{
  double lat_x = 0.0;
  double lon_y = 0.0;
  double elevation = 0.0;

  //! @brief display the gps datas
  void display(){
    fmt::print("lat_x: {}\tlon_y: {}\n",lat_x,lon_y);
  }

  //! @brief serialze the gps datas and push back
  //!
  //! @param target
  //!
  //! @return total size of gps datas
  size_t serialize(std::vector<char> &target){
    ::serialize<double>(lat_x,target);
    ::serialize<double>(lon_y,target);
    ::serialize<double>(elevation,target);

    return 3*sizeof(double);
  }

  //! @brief deserialize the gps datas and store
  //!
  //! @param target
  //!
  //! @return total size of gps datas 
  size_t deserialize(std::vector<char>::iterator target){
    lat_x = *reinterpret_cast<double*>(target.base());
    lon_y = *reinterpret_cast<double*>((target+sizeof(lat_x)).base());
    elevation = *reinterpret_cast<double*>((target+sizeof(lat_x)+sizeof(lon_y)).base());

    return sizeof(lat_x)+sizeof(lon_y)+sizeof(elevation);
  }
} GpsData;

//! @brief simulated ego vehicle inertia
typedef struct ImuData{
  //! GYRO_PITCH, GYRO_ROLL, GYRO_YAW
  double gyro[3] = {0.0, 0.0, 0.0};
  //! ACC_X, ACC_Y, ACC_Z
  double accel[3] = {0.0, 0.0, 0.0};
  double heading = 0;
  
  //! @brief data insert with arrays
  //!
  //! @param datas[7]
  void dataInsert(double datas[7]){
    for(int i=0; i<3; ++i)
      gyro[i] = datas[i];
    for(int i=0; i<3; ++i)
      accel[i] = datas[3+i];
    heading = datas[6];
  }

  //! @brief display the imu datas
  void display(){
    fmt::print("pitch: {}\troll: {}\tyaw: {}\nacc x: {}\tacc y: {}\tacc z: {}\ncompass: {}\n",
        gyro[0], gyro[1], gyro[2], accel[0], accel[1], accel[2], heading);
  }

  //! @brief serialize the imu datas and push back to the target buffer
  //!
  //! @param target
  //!
  //! @return total size of imu datas 
  size_t serialize(std::vector<char> &target){
    for(int i=0; i<3; ++i)
      ::serialize<double>(gyro[i],target);
    for(int i=0; i<3; ++i)
      ::serialize<double>(accel[i],target);
    ::serialize<double>(heading,target);
    
    return 7*sizeof(double);
  }

  //! @brief deserialize the imu datas and store
  //!
  //! @param target
  //!
  //! @return total size of imu datas
  size_t deserialize(std::vector<char>::iterator target){
    for(int i = 0; i<3; ++i){
      gyro[i] = ::deserialize<double>(target);
      target += sizeof(double);
    }
    for(int i = 0; i<3; ++i){
      accel[i] = ::deserialize<double>(target);
      target += sizeof(double);
    }
    heading = ::deserialize<double>(target);

    return 7*sizeof(double);
  }
} ImuData;

//! @brief simulated ego vehicle states
typedef struct SimulationVehicleState{
  SimulatorTimeTick measure_time;
  GpsData gps_data;
  ImuData imu_data;
  double speed = 0.0;
  BrakeState break_state = BrakeState::BRAKE_NONE;
  double steer = 0.0;
  DrivingMode driving_mode_state = DrivingMode::DRIVING_NONE_EXCEPTION;
  GearState gear_state = GearState::GEAR_NONE_EXCEPTION;
  //! @brief if the gear_state isn't 4, gear_level should be 0.
  //!  if the gear_state is 4, gear_level is more then 1.
  unsigned char gear_level = 0;
  BlinkerState blinker_state = BlinkerState::BLINKER_NONE_EXCEPTION;

  //! @brief display all elements of this structure
  void display(){
    fmt::print("simulated vehicle state\n");
    measure_time.display();
    gps_data.display();
    imu_data.display();
    fmt::print("speed: {}\tbreak: {}\tsteer: {}\ndriving mode: {}\tgear: {}\t{}\nblinker: {}\n",
        speed, break_state, steer, driving_mode_state, gear_state, gear_level, blinker_state);
  }
  
  //! @brief serialize the vehicle state datas and push back to the target buffer
  //!
  //! @param target
  //!
  //! @return total size of the vehicle state datas
  size_t serialize(std::vector<char> &target){
    size_t data_size = 0;
    data_size += measure_time.serialize(target);
    data_size += gps_data.serialize(target);
    data_size += imu_data.serialize(target);
    data_size += ::serialize<double>(speed,target);
    data_size += ::serialize<BrakeState>(break_state,target);
    data_size += ::serialize<double>(steer,target);
    data_size += ::serialize<DrivingMode>(driving_mode_state,target);
    data_size += ::serialize<GearState>(gear_state,target);
    data_size += ::serialize<unsigned char>(gear_level,target);
    data_size += ::serialize<BlinkerState>(blinker_state,target);
    return data_size;
  }

  //! @brief deserialize the vehicle state datas and store
  //!
  //! @param target
  //!
  //! @return total size of the vehicle state datas
  size_t deserialize(std::vector<char>::iterator target){
    size_t data_idxer = 0;
    data_idxer += measure_time.deserialize(target);
    data_idxer += gps_data.deserialize(target+data_idxer);
    data_idxer += imu_data.deserialize(target+data_idxer);
    
    speed = *reinterpret_cast<double*>((target+data_idxer).base());
    data_idxer += sizeof(speed);

    break_state = *reinterpret_cast<BrakeState*>((target+data_idxer).base());
    data_idxer += sizeof(break_state);

    steer = *reinterpret_cast<double*>((target+data_idxer).base());
    data_idxer += sizeof(steer);

    driving_mode_state = *reinterpret_cast<DrivingMode*>((target+data_idxer).base());
    data_idxer += sizeof(driving_mode_state);

    gear_state = *reinterpret_cast<GearState*>((target+data_idxer).base());
    data_idxer += sizeof(gear_state);

    gear_level = *reinterpret_cast<unsigned char*>((target+data_idxer).base());
    data_idxer += sizeof(gear_level);

    blinker_state = *reinterpret_cast<BlinkerState*>((target+data_idxer).base());
    data_idxer += sizeof(blinker_state);
    
    return data_idxer;
  }
} SimulationVehicleState;

//! @brief commection information about the KETI ROS bridge. not used now.
//!  if the other node should connect to the KETI ROS bridge, use this datas.
//!  in this case, of course, bridge should have any acceptors.
typedef struct BridgeConnectionInfo{
  std::string host_name;
  std::string ip_address;
  std::vector<int> acceptable_port;

  BridgeConnectionInfo(){}
  ~BridgeConnectionInfo(){
    host_name.clear();
    ip_address.clear();
    acceptable_port.clear();
  }

  //! @brief set bridge connection name and ip address
  //!
  //! @param name
  //! @param ip
  void setNameIp(std::string name, std::string ip){
    host_name = name;
    ip_address = ip;
  }

  //! @brief add a acceptable port number
  //!
  //! @param port
  void addPort(int port){
    acceptable_port.push_back(port);
  }

  //! @brief display datas
  void display(){
    fmt::print("bridge information\nhost_name: {}\nip address: {}\nacceptable port number: {}\n",host_name, ip_address, acceptable_port.size());
  }

  //! @brief serialize the bridge connection information and push back to the target buffer
  //!
  //! @param target
  //!
  //! @return total size of bridge coneection information
  size_t serialize(std::vector<char> &target){
    size_t tmp_size = 0;

    // tmp_size += ::serialize<unsigned int>((unsigned int)host_name.size(), target);
    std::copy(host_name.begin(),host_name.end(),std::back_inserter(target));
    tmp_size += host_name.size();

    // tmp_size += ::serialize<unsigned int>((unsigned int)ip_address.size(), target);
    std::copy(ip_address.begin(),ip_address.end(),std::back_inserter(target));
    tmp_size += ip_address.size();

    // tmp_size += ::serialize<unsigned int>((unsigned int)acceptable_port.size(),target);

    for(size_t i=0;i<acceptable_port.size();++i)
    {
      tmp_size += ::serialize<int>(acceptable_port[i],target);
    }

    return tmp_size;
  }

  //! @brief deserialize bridge connection information
  //!
  //! @param target
  void deserialize(std::vector<char>::iterator target){
    unsigned int name_size = ::deserialize<unsigned int>(target);
    target += sizeof(name_size);
    if(name_size>0)
      host_name = std::string(target,target+name_size);
    target += name_size;
    unsigned int ip_size = ::deserialize<unsigned int>(target);
    target += sizeof(ip_size);
    if(ip_size>0)
      ip_address = std::string(target,target+ip_size);
    target += ip_size;
    unsigned int port_size = ::deserialize<unsigned int>(target);
    target += sizeof(port_size);
    acceptable_port.clear();
    for(unsigned int i = 0; i < port_size; ++i){
      acceptable_port.push_back(::deserialize<int>(target));
      target += sizeof(int);
    }
  }
} BridgeConnectionInfo;

//! @brief converted ego vehicle control datas
typedef struct SimulationControlData{
  SimulatorTimeTick measure_time;
  double input_pedal_accel = 0.0;
  double steer = 0.0;
  DrivingMode driving_mode_state = DrivingMode::DRIVING_NONE_EXCEPTION;
  GearState gear_state = GearState::GEAR_NONE_EXCEPTION;
  BlinkerState blinker_state = BLINKER_NONE_EXCEPTION;

  //! @brief cleanup the data
  void flush(){
    measure_time.time_sec = 0;
    measure_time.time_nsec = 0;
    input_pedal_accel = 0;
    steer = 0;
    driving_mode_state = DrivingMode::DRIVING_NONE_EXCEPTION;
    gear_state = GearState::GEAR_NONE_EXCEPTION;
    blinker_state = BlinkerState::BLINKER_NONE_EXCEPTION;
  }

  //! @brief display datas
  void display(){
    fmt::print("display control data!!\n");
    measure_time.display();
    fmt::print("input pedal_accel: {}\tsteer: {}\n", input_pedal_accel, steer);
    fmt::print("driving mode: {}\tgear state: {}\tblinker: {}\n", driving_mode_state,
        gear_state, blinker_state);
  }

  //! @brief serialize the control datas and push back to the target buffer
  //!
  //! @param target
  //!
  //! @return control data size 
  size_t serialize(std::vector<char> &target){
    size_t tmp_size = 0;
    tmp_size += measure_time.serialize(target);
    tmp_size += ::serialize<double>(input_pedal_accel,target);
    tmp_size += ::serialize<double>(steer,target);
    tmp_size += ::serialize<DrivingMode>(driving_mode_state,target);
    tmp_size += ::serialize<GearState>(gear_state,target);
    tmp_size += ::serialize<BlinkerState>(blinker_state,target);

    return tmp_size;
  }

  //! @brief deserialize the control datas and store
  //!
  //! @param target
  void deserialize(std::vector<char>::iterator target){
    auto target_it = target + measure_time.deserialize(target);
    input_pedal_accel = ::deserialize<double>(target_it);
    target_it += sizeof(double);
    steer = ::deserialize<double>(target_it);
    target_it += sizeof(double);
    driving_mode_state = ::deserialize<DrivingMode>(target_it);
    target_it += sizeof(driving_mode_state);
    gear_state = ::deserialize<GearState>(target_it);
    target_it += sizeof(gear_state);
    blinker_state = ::deserialize<BlinkerState>(target_it);
  }
} SimulationControlData;

//! @brief simulator core state structure
typedef struct SimulationServerState {
  SimulationRunningState running_state;
  SimulationVehicleDataType vehicle_type;
  SimulationWeatherState weather_condition;
  SimulatorTimeTick current_time;
  std::vector<SensorConnectionInfo> sensor_list;

  SimulationServerState(){};
  ~SimulationServerState(){
    sensor_list.clear();
  }

  //! @brief serialize the simulator server state and push back
  //!
  //! @param target
  //!
  //! @return total size of the simulator server state
  size_t serialize(std::vector<char> &target){
    std::vector<char> tmp_buff;
    unsigned int start_idx = 35;
    unsigned int tmp_size = 0;
    SimInfoTable addr_table;
    ::serialize<SimulationRunningState>(running_state,tmp_buff);
    addr_table[0] = start_idx;
    start_idx += sizeof(SimulationRunningState);
    tmp_size = (unsigned int)vehicle_type.serialize(tmp_buff);
    addr_table[1] = start_idx;
    start_idx += tmp_size;
    tmp_size = (unsigned int)weather_condition.serialize(tmp_buff);
    addr_table[2] = start_idx;
    start_idx += tmp_size;
    tmp_size = current_time.serialize(tmp_buff);
    addr_table[3] = start_idx;
    start_idx += tmp_size;
    ::serialize<unsigned int>(sensor_list.size(),tmp_buff);
    for(size_t i=0;i<sensor_list.size();++i)
      sensor_list[i].serialize(tmp_buff);
    addr_table[4] = start_idx;
    addr_table[5] = addr_table[6] = 0;
    for(char i=0;i<7;++i){
      target.push_back(i+1);
      ::serialize<unsigned int>(addr_table[i],target);
    }
    std::copy(tmp_buff.begin(),tmp_buff.end(),std::back_inserter(target));
    return tmp_buff.size();
  }
  
  //! @brief deserialize the simulator server state and store
  //!
  //! @param table
  //! @param buff
  void deserialize(SimInfoTable table, std::vector<char>::iterator buff){
    for(size_t i=0;i<5;++i){
      if(table[i] != 0){
        switch(i){
          case SimulationInformation::INFO_SIMULATION_RUNNING:
            running_state = ::deserialize<SimulationRunningState>(buff + table[i]);
            break;
          case SimulationInformation::INFO_SIMULATION_VEHICLE_DATA_TYPE:
            vehicle_type.deserialize(buff + table[i]);
            break;
          case SimulationInformation::INFO_SIMULATION_WEATHER_STATE:
            weather_condition.deserialize(buff + table[i]);
            break;
          case SimulationInformation::INFO_SIMULATION_TIME_TICK:
            current_time.deserialize(buff + table[i]);
            break;
          case SimulationInformation::INFO_EMULATED_SENSOR_CONNECTION:
            auto target_it = buff+table[i];
            unsigned int array_size = ::deserialize<unsigned int>(target_it);
            if(array_size > 0)
              sensor_list.resize(array_size);
            target_it += sizeof(array_size);
            for(unsigned int i=0;i<array_size;++i)
              target_it += sensor_list[i].deserialize(target_it);
            break;
        }
      }
    }
  }

} SimulationServerState;

#endif
