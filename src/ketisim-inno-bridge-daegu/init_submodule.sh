SHLINK=$(readlink -f "$0")
SHPATH=$(dirname "$SHLINK")
echo $SHPATH

cd $SHPATH

sudo apt update
sudo apt install libboost-all-dev libopenblas-base libopenblas-dev liblapack3 liblapack-dev autoconf automake libtool curl make g++ unzip

git submodule update --init --recursive

cd $SHPATH/modules/fmt
mkdir build && cd build
cmake ..
make -j -l && sudo make install

cd $SHPATH/modules/protobuf
./autogen.sh
./configure --prefix=/usr/local
make -j -l && make check
sudo make install
sudo ldconfig

cd $SHPATH/modules/open-simulation-interface
sed -i '4i\set(CMAKE_CXX_STANDARD 14)' CMakeLists.txt
sed -i '5i\set(CMAKE_CXX_STANDARD_REQUIRED ON)' CMakeLists.txt
sed -i '6i\set(CMAKE_CXX_EXTENSIONS OFF)' CMakeLists.txt
mkdir build && cd build
cmake -DFILTER_PROTO2CPP_PY_PATH=../../proto2cpp ..
make -j -l
sudo make install

cd $SHPATH/modules/blaze
cmake -DCMAKE_INSTALL_PREFIX=/usr/local/
sudo make install

cd $SHPATH/../..
source ../common/devel/setup.bash
catkin_make