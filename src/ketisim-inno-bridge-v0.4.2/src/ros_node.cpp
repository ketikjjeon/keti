#include "ros_node.h"

//! @brief default constructor
//!
//! @param argc
//! @param argv
//! @param io_serv
RosManager::RosManager(int argc, char** argv, io_service_sptr io_serv, std::shared_ptr<ros::NodeHandle> sptr_nh, std::string ip, int port, bool is_receive_sensor) :
    TCPClient(io_serv, 12),
    simulator_ip_(ip),
    simulator_port_(port),
    sptr_nh_(sptr_nh),
    data_flag_(0),
    server_conn_chk_(false){

  receive_header_checker_.resize(3);
  receive_header_checker_[0] = 0x48;
  receive_header_checker_[1] = 0x53;
  receive_header_checker_[2] = 0x57;

  sptr_nh_->param("use_sim_time", use_sim_time_, bool(false));
  if(use_sim_time_)
    sim_time_convert_ = std::make_unique<OsiRosTimer>(sptr_nh_, osiNros::TopicInfoArr(), use_sim_time_);
  sptr_nh_->param("use_compressed_img", use_compressed_img_, bool(true));

  sptr_nh_->param("/x_offset", gps_offset_x_, double(0.0));
  sptr_nh_->param("/y_offset", gps_offset_y_, double(0.0));

  std::cout.precision(3);
  std::cout << std::fixed << gps_offset_x_ << ", " << gps_offset_y_ << std::endl;

  std::string tmp_control_topic_name, tmp_config_topic_name, tmp_gt_topic_name;
  sptr_nh_->param("control_name", tmp_control_topic_name, std::string("/vehicle_cmd"));
  sptr_nh_->param("config_name", tmp_config_topic_name, std::string("/vehicle_config"));
  sptr_nh_->param("use_gt", use_gt_data_, bool(true));
  sptr_nh_->param("GT_name", tmp_gt_topic_name, std::string("/gt_objs"));

  fmt::print(stdout, "simulator connection addr: {}:{}\n", simulator_ip_, simulator_port_);
  osiNros::TopicInfoArr tmp_vehicle_state;
  tmp_vehicle_state.emplace_back(0, "/current_pose", osiNros::TopicType::TOPIC_POSITION, 
      "geometry_msgs::PoseStamped", osiNros::SensorType::SENSOR_GNSS_IMU);
  tmp_vehicle_state.emplace_back(0, "/current_pose_odom", osiNros::TopicType::TOPIC_POSITION,
      "nav_msgs::Odometry", osiNros::SensorType::SENSOR_GNSS_IMU);
  tmp_vehicle_state.emplace_back(0, "/vehicle_state", osiNros::TopicType::TOPIC_VEHICLE_STATE,
      "control_msgs::VehicleState");

  osiNros::TopicInfoArr tmp_control;
  tmp_control.emplace_back(0, tmp_control_topic_name, osiNros::TopicType::TOPIC_CONTROL,
      "control_msgs::VehicleCMD");
  tmp_control.emplace_back(0, tmp_config_topic_name, osiNros::TopicType::TOPIC_CONTROL,
      "control_msgs::VehicleConfig");
  
  control_convert_ = std::make_unique<OsiRosControl>(sptr_nh_, tmp_control, use_sim_time_);
  state_convert_ = std::make_unique<OsiRosVehicle>(sptr_nh_, tmp_vehicle_state, use_sim_time_,
      gps_offset_x_, gps_offset_y_);

  if (is_receive_sensor)
  {
    if(use_gt_data_){
      osiNros::TopicInfoArr tmp_gt;
      tmp_gt.emplace_back(0, tmp_gt_topic_name, osiNros::TopicType::TOPIC_GT, 
          osiNros::getTopicType(osiNros::TOPIC_GT));
      gt_converter_ = std::make_unique<OsiRosGT>(sptr_nh_, tmp_gt, use_sim_time_);
    }
    
    osiNros::TopicInfoArr tmp_camera_info_;
    osiNros::TopicInfoArr tmp_lidar_info_;
    osiNros::TopicInfoArr tmp_radar_info_;

    if(!readSensorConfig()){
      fmt::print(stderr,"There is not a osi to ros sensor name matching file.\nuse default setting.\n");
    } else{
      for(size_t i=0; i<topic_setting_.size(); ++i){
        switch(topic_setting_[i]->sensor_type){
          case osiNros::SENSOR_CAMERA:
            tmp_camera_info_.push_back(*topic_setting_[i]);
            break;
          case osiNros::SENSOR_LIDAR:
            tmp_lidar_info_.push_back(*topic_setting_[i]);
            break;
          case osiNros::SENSOR_RADAR:
            tmp_radar_info_.push_back(*topic_setting_[i]);
            break;
          case osiNros::SENSOR_ULTRASONIC:
            fmt::print(stderr,"ultrasonic is not supported sensor yet.\n"); 
            break;
          case osiNros::SENSOR_GNSS_IMU:
            fmt::print(stderr,"external gnss and imu are not supported sensor yet.\n");
            break;
          default:
            fmt::print(stderr,"ERROR Unknown sensor type : {}.\ncheck the setting file.\n", topic_setting_[i]->sensor_type);
            break;
        }
      }
    }
    if(tmp_camera_info_.size() > 0)
      sensor_camera_convert_ = std::make_unique<OsiRosCamera>(sptr_nh_, tmp_camera_info_, use_sim_time_);
    if(tmp_lidar_info_.size() > 0)
      sensor_lidar_convert_ = std::make_unique<OsiRosLidar>(sptr_nh_, tmp_lidar_info_, use_sim_time_);
    if(tmp_radar_info_.size() > 0)
      sensor_radar_convert_ = std::make_unique<OsiRosRadar>(sptr_nh_, tmp_radar_info_, use_sim_time_);
  }
}

//! @brief default destructor
RosManager::~RosManager(){
  topic_setting_.clear();
}

//! @brief ros node starter
void RosManager::run(){
  connect();
  control_convert_->setSendFunction(std::bind(&RosManager::_send, this, 
        std::placeholders::_1));
}

//! @brief simulator packet header checker
//!
//! @return size of packet body
size_t RosManager::headerChecker(){
  if((m_receive_header_[0] == 0x48) &&
     (m_receive_header_[1] == 0x53) &&
     (m_receive_header_[2] == 0x57))
     {
      uint64_t size_tmp = 0;
      data_flag_ = m_receive_header_[3];

      switch(data_flag_){
        case DataClassFlag::FLAG_ERROR_UNKNOWN:
          fmt::print(stderr, "received packet class is ERROR & UNKNOWN\n");
          break;
        case DataClassFlag::FLAG_SERVER_INFO:
          size_tmp = *reinterpret_cast<uint64_t*>(&m_receive_header_[4]);
          break;
        case DataClassFlag::FLAG_CLIENT_INFO:
          fmt::print(stderr, "received packet class is CLIENT INFO\n");
          break;
        case DataClassFlag::FLAG_TIME_TICK:
          size_tmp = *reinterpret_cast<uint64_t*>(&m_receive_header_[4]);
          break;
        case DataClassFlag::FLAG_EMULATED_DATA:
          size_tmp = *reinterpret_cast<uint64_t*>(&m_receive_header_[4]);
          break;
        case DataClassFlag::FLAG_VEHICLE_STATUS:
          size_tmp = *reinterpret_cast<uint64_t*>(&m_receive_header_[4]);
          break;
        case DataClassFlag::FLAG_CONTROL_INPUT:
          fmt::print(stderr, "received packet class is CONTROL INPUT\n");
          break;
        default:
          fmt::print(stderr, "received packet class is not defined: {}\n", m_receive_header_[3]);
          break;
      }

      return size_tmp;
  } else
    return 0;
}


//! @brief callback packet body
void RosManager::receiveCallback(){
  switch(data_flag_){
    case DataClassFlag::FLAG_ERROR_UNKNOWN:
      fmt::print(stderr, "received ERROR/UNKNOWN flag data!!\npass the packet body\n");
      break;
    case DataClassFlag::FLAG_SERVER_INFO :{
      server_conn_chk_ = true;
      
      SimInfoTable simulation_info_addr;
      auto body_iter = m_receive_body_.begin();
      for(size_t i=0; i<7;++i){
        simulation_info_addr[i] = ::deserialize<unsigned int>(
            (body_iter + (i*5) + 1));
      }
      if(simulation_info_addr[0] > 0)
        simulation_running_ = (bool)*(body_iter + simulation_info_addr[0]);
      if(simulation_info_addr[1] > 0)
        sim_data_type_.deserialize(body_iter + simulation_info_addr[1]);
      if(simulation_info_addr[2] > 0)
        sim_weather_.deserialize(body_iter + simulation_info_addr[2]);
      if(simulation_info_addr[3] > 0 && use_sim_time_){
        SimulatorTimeTick timetick;
        timetick.deserialize(body_iter + simulation_info_addr[3]);
        sim_time_convert_->update(timetick.time_sec, timetick.time_nsec);
      }
      if(simulation_info_addr[4] > 0)
        sim_sensor_info_.deserialize(body_iter + simulation_info_addr[4]);
      if(simulation_info_addr[5] > 0)
        state_convert_->callback(body_iter + simulation_info_addr[5], sim_data_type_.gps_type);
      if(simulation_info_addr[6] > 0)
        fmt::print(stderr, "Received bridge connection info datas.\n pass the packet body.\n");

      break;}
    case DataClassFlag::FLAG_CONTROL_INPUT:
      fmt::print(stderr, "received Vehicle Control flag data!!\npass the packet body\n");
      break;
    case DataClassFlag::FLAG_EMULATED_DATA:{
      if(m_receive_body_.size() > 11)
      {
        int tmp_osi_type = *reinterpret_cast<int*>(m_receive_body_.begin().base());
        int tmp_proto_size = *reinterpret_cast<int*>((m_receive_body_.begin()+4).base());
        int tmp_raw_data_size = *reinterpret_cast<int*>((m_receive_body_.begin()+8).base());       

        switch(tmp_osi_type){
          case SENSOR_CAMERA:
            sensor_camera_convert_->deserialize((m_receive_body_.begin()+12), tmp_proto_size,
                tmp_raw_data_size);
            break;
          
          case SENSOR_LIDAR:            
            sensor_lidar_convert_->deserialize((m_receive_body_.begin()+12), tmp_proto_size,
                tmp_raw_data_size);
            break;

          case SENSOR_GT:
            if(use_gt_data_){
              gt_converter_->deserialize((m_receive_body_.begin()+12), tmp_proto_size,
                  tmp_raw_data_size);
            }
            break;
          
          case SENSOR_RADAR:
            sensor_radar_convert_->deserialize((m_receive_body_.begin()+12), tmp_proto_size,
                tmp_raw_data_size);
            break;

          default:
            fmt::print(stderr,"Unkown sensor type datas! : {} \n", tmp_osi_type);
            break;
        }
      } else{
        fmt::print(stderr, "received sensor header error!!\n");
        fmt::print(stderr, "sensor data hex: {}\n", hexStr(m_receive_body_.begin().base(), m_receive_body_.size()));
      }
      break;}
    default:
      fmt::print(stderr,"received packet body is undefined datas!!\n");
      break;
  }

}

//! @brief connect to simulator & handshake
void RosManager::connect(){
  bool chk = _connect(simulator_ip_, simulator_port_);
  if(chk){
    std::vector<char> tmp_buff = {0x48, 0x53, 0x57, 0x00};
    tmp_buff.resize(12);
    BridgeConnectionInfo client_info;
    tmp_buff[3] = DataClassFlag::FLAG_SERVER_INFO;

    SimInfoTable tmp_table;
    tmp_table.fill(0);
    tmp_table[SimulationInformation::INFO_BRIDGE_CONNECTION_INFO] = 35;
    ::serialTableSize(tmp_table, tmp_buff);

    client_info.setNameIp(std::string("KETI_SIM_AI_BRIDGE"),std::string("192.168.56.102"));
    client_info.addPort(7878);

    size_t clientinfo_size = client_info.serialize(tmp_buff);
    ::serialSizeApply(clientinfo_size, tmp_buff);

    
    _send(std::move(tmp_buff));
  }
}

//! @brief read sensor configuration file to naming topic & classification
//!
//! @return exist file
bool RosManager::readSensorConfig(){
  std::string full_path = ros::package::getPath("ketisim_inno_bridge");
  full_path += std::string("/config/sensor.csv");

  std::ifstream file_handle;
  std::cout << "file full path: " << full_path << std::endl;
  file_handle.open(full_path.c_str(), std::ifstream::in);
  if(!file_handle.is_open()){
    ROS_WARN("sensor matching configuration file does not exist! check the file!");
    topic_setting_.clear();
    return false;
  } else{
    std::string line_value;
    std::string token_value;
    osiNros::SensorType tmp_sensor_type;
    unsigned int tmp_id;
    while(std::getline(file_handle,line_value)){
      std::istringstream tokenization(line_value);
      token_value.clear();
      std::getline(tokenization,token_value,',');
      tmp_id = std::stod(token_value);
      token_value.clear();
      std::getline(tokenization,token_value,',');
      tmp_sensor_type = osiNros::SensorType(std::stod(token_value));
      token_value.clear();
      std::getline(tokenization,token_value,',');
      std::string tmp_name = std::string("/") + token_value;

      switch(tmp_sensor_type){
        case osiNros::SENSOR_CAMERA:
          if ( use_compressed_img_ )
            topic_setting_.emplace_back(tmp_id, tmp_name+std::string("/image_raw/compressed"), osiNros::TOPIC_COMPRESSED_IMAGE,
                tmp_sensor_type);
          else
            topic_setting_.emplace_back(tmp_id, tmp_name+std::string("/image_raw"), osiNros::TOPIC_RAW_IMAGE,
                tmp_sensor_type);
          topic_setting_.emplace_back(tmp_id, tmp_name+std::string("/camera_info"), osiNros::TOPIC_CAMERA_INFO,
              tmp_sensor_type);
          break;
        case osiNros::SENSOR_LIDAR:
          topic_setting_.emplace_back(tmp_id, tmp_name, osiNros::TOPIC_POINTCLOUD2, tmp_sensor_type);
          break;
        case osiNros::SENSOR_RADAR:
          topic_setting_.emplace_back(tmp_id, tmp_name, osiNros::TOPIC_RADAR, tmp_sensor_type);
          // fmt::print(stderr, "radar is not supported yet!\n");
          break;
        case osiNros::SENSOR_ULTRASONIC:
          fmt::print(stderr, "ultrasonic is not supported yet!\n");
          break;
        case osiNros::SENSOR_GNSS_IMU:
          fmt::print(stderr, "gnss / imu external sensor is not supported yet!\n");
          break;
        default:
          break;
      }
    }
    ROS_INFO("Load converting information file complete");
    file_handle.close();
  }
  return true;
}

int main(int argc, char **argv){
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  ros::init(argc, argv, "ketisim_inno_bridge");

  io_service_sptr io_serv_inno = std::make_shared<io_service>();
  io_service_sptr io_serv_sim = std::make_shared<io_service>();

  std::shared_ptr<ros::NodeHandle> sptr_nh = std::make_shared<ros::NodeHandle>("~");

  std::string inno_ip;
  int inno_port;
  sptr_nh->param("inno_ip", inno_ip, std::string("127.0.0.1"));
  sptr_nh->param("inno_port", inno_port, int(20000));

  std::shared_ptr<RosManager> rosmanager_inno = std::make_shared<RosManager>(argc, argv, io_serv_inno, sptr_nh, inno_ip, inno_port, false);

  std::string sim_ip;
  int sim_port;
  sptr_nh->param("simulator_ip", sim_ip, std::string("127.0.0.1"));
  sptr_nh->param("simulator_port", sim_port, int(7878));

  std::shared_ptr<RosManager> rosmanager_sim = std::make_shared<RosManager>(argc, argv, io_serv_sim, sptr_nh, sim_ip, sim_port, true);

  rosmanager_inno->run();
  rosmanager_sim->run();

  // ros::Rate loop_rate(1000);
  // while(ros::ok())
  // {
  //   ros::spinOnce();
  //   loop_rate.sleep();
  // }

  ros::Rate loop_rate(1000);
  while(ros::ok())
  {
    std::size_t ran = 0;
    ros::spinOnce();
    
    ran += io_serv_inno->poll();
    ran += io_serv_sim->poll();

    if (0 == ran)
    {
      // boost::this_thread::sleep_for(boost::chrono::seconds(1));
      loop_rate.sleep();
    }
  }

  google::protobuf::ShutdownProtobufLibrary();
  return 0;
}

