/**
 * @file network_client.cpp
 *
 * @brief This header include ethernet TCP communication based
 *  endpoint client class.
 *
 * @author Sangwook Ha
 *  swid1@keti.re.kr
 *
 */

#include "network_client.h"


//! @brief byte array to the hex string for debugging
//!
//! @param data
//! @param len
//!
//! @return hex string
std::string hexStr(char *data, int len){
  constexpr char hexmap[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                             '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
  std::string s(len * 3, ' ');
  for (int i = 0; i < len; ++i){
    s[3*i] = hexmap[(data[i] & 0xF0) >> 4];
    s[3*i+1] = hexmap[data[i] & 0x0F];
    s[3*i+2] = ' ';
  }
  return s;
}

//! @brief default constructor
//!
//! @param io_service
//! @param header_size
TCPClient::TCPClient(io_service_sptr io_service, size_t header_size) :
    m_receive_header_(header_size),
    header_size_(header_size),
    m_io_service_(io_service), 
    m_socket_(*io_service){
}

//! @brief move constructor
//!
//! @param t
TCPClient::TCPClient(TCPClient &&t) :
    m_receive_header_(std::move(t.m_receive_header_)),
    m_receive_body_(std::move(t.m_receive_body_)),
    receive_header_checker_(t.receive_header_checker_),
    curr_body_(t.curr_body_),
    header_size_(t.header_size_),
    size_temp_(t.size_temp_),
    io_thread_(std::move(t.io_thread_)),
    m_io_service_(t.m_io_service_),
    m_socket_(std::move(t.m_socket_)),
    send_packet_buff_(std::move(t.send_packet_buff_)){
}

//! @brief default destructor
TCPClient::~TCPClient(){
  m_receive_body_.clear();
  m_receive_header_.clear();
  receive_header_checker_.clear();
  send_packet_buff_.clear();
  if(isConnected())
    m_io_service_->post([this](){this->m_socket_.close();});
  io_thread_.join();
}

//! @brief connect to the target listener
//!
//! @param ip
//! @param port
//!
//! @return result of open 
bool TCPClient::_connect(const std::string &ip, const std::string &port){
  endpoint_uptr_ = std::make_unique<tcp::endpoint>(
    boost::asio::ip::address::from_string(ip), std::stoi(port));
  return _connect();
}

//! @brief connect to the target listener
//!
//! @param ip
//! @param port
//!
//! @return result of open
bool TCPClient::_connect(std::string &ip, int port){
  endpoint_uptr_ = std::make_unique<tcp::endpoint>(
    boost::asio::ip::address::from_string(ip), port);
  return _connect();
}

//! @brief disconnect the current session
void TCPClient::_disconnect(){
  m_io_service_->post([this](){m_socket_.close();});
  io_thread_.join();
}


//! @brief check the connection is open
//!
//! @return current state of this session
bool TCPClient::isConnected(){
  return m_socket_.is_open();
} 

//! @brief add char type vector buffer to the send queue
//!
//! @param data
//!
//! @return result
bool TCPClient::_send(std::vector<char> &&data){
  if(data.size() < 1)
    return false;
  
  auto self(shared_from_this());
  std::vector<char> tmp = data;
  m_io_service_->post([self, tmp](){
    bool send_progress = self->send_packet_buff_.empty();
    self->send_packet_buff_.push_back(std::move(tmp));
    if(send_progress)
      self->dataSend();
    });
  return true;
}

//! @brief move operator
//!
//! @param t
//!
//! @return *this
TCPClient& TCPClient::operator=(TCPClient &&t){
  m_receive_header_ = std::move(t.m_receive_header_);
  m_receive_body_ = std::move(t.m_receive_body_);
  curr_body_ = t.curr_body_;
  header_size_ = t.header_size_;
  size_temp_ = t.size_temp_;
  io_thread_ = std::move(t.io_thread_);
  m_io_service_ = std::move(t.m_io_service_);
  m_socket_ = std::move(t.m_socket_);
  send_packet_buff_ = std::move(t.send_packet_buff_);
  return *this;
}

//! @brief async send data from the send buffer
void TCPClient::dataSend(){
  auto self(shared_from_this());
  boost::asio::async_write(m_socket_,boost::asio::buffer(
    send_packet_buff_.front().data(),send_packet_buff_.front().size()),
    [self](boost::system::error_code ec, std::size_t ){
      if(!ec){

        self->send_packet_buff_.pop_front();
        if(!self->send_packet_buff_.empty())
          self->dataSend();
      } else{
        self->m_socket_.close();
        fmt::print("error No. {}\terror message: {}\n", ec.value(),ec.message());
      }
    });
}

//! @brief receive packet header
void TCPClient::startReceive(){
  auto self(shared_from_this());
  std::fill(m_receive_header_.begin(), m_receive_header_.end(),0);
  if(isConnected() && header_size_ > 0)
    boost::asio::async_read(m_socket_,boost::asio::buffer(m_receive_header_,header_size_),
      boost::asio::transfer_at_least(header_size_),boost::bind(&TCPClient::HandleReceive, this,
      boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, self));
}

void TCPClient::startReceive(size_t head_size){
  auto self(shared_from_this());
  if(isConnected() && header_size_ > 0)
    boost::asio::async_read(m_socket_,boost::asio::buffer((m_receive_header_.begin() + 
            header_size_ - head_size).base(),header_size_),
      boost::asio::transfer_at_least(header_size_), boost::bind(&TCPClient::HandleReceive, this,
      boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, self));
}

//! @brief receive packet body
//!
//! @param recv_size
void TCPClient::recvBody(size_t recv_size, std::shared_ptr<TCPClient> self){
  boost::asio::async_read(m_socket_,boost::asio::buffer((self->m_receive_body_.begin() + self->curr_body_).base(),
    recv_size), boost::asio::transfer_at_least(recv_size),boost::bind(&TCPClient::handleBody, 
    this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, self));
}

//! @brief process the packet header
//!
//! @param ec
//! @param bytes_transferred
void TCPClient::HandleReceive(const boost::system::error_code& ec, size_t bytes_transferred, std::shared_ptr<TCPClient> self){
  if(!ec){
    self->size_temp_ = self->headerChecker();
    if(self->size_temp_ > 0){
      self->m_receive_body_.clear();
      self->m_receive_body_.resize(self->size_temp_);
      self->curr_body_ = 0;
      self->recvBody(self->size_temp_, self);
    } else{
      fmt::print(stderr, "header receive error.\ntransfered size: {} decoded size: {}\nreceived datas: {} \n error msg : {} \n",
          bytes_transferred, self->size_temp_, hexStr(self->m_receive_header_.begin().base(), self->header_size_), ec.message());
      if(self->receive_header_checker_.size() > 0){
        auto tmp_iter = std::search(self->m_receive_header_.begin(), self->m_receive_header_.end(),
            self->receive_header_checker_.begin(), self->receive_header_checker_.end());
        if(tmp_iter != self->m_receive_header_.end()){
          size_t rm_size = tmp_iter - self->m_receive_header_.begin();
          self->m_receive_header_.erase(self->m_receive_header_.begin(), tmp_iter);
          self->m_receive_header_.resize(self->header_size_);
          self->startReceive(rm_size);
        } else
          self->startReceive();
      } else
        self->startReceive();
    }
  } else{
    if(ec == boost::asio::error::connection_reset || ec == boost::asio::error::eof)
      m_socket_.close();
    fmt::print("error No. {}\terror message: {}\n", ec.value(), ec.message());
  }
}

//! @brief process the packet body
//!
//! @param ec
//! @param bytes_transferred
void TCPClient::handleBody(const boost::system::error_code& ec, size_t bytes_transferred, std::shared_ptr<TCPClient> self){
  if(!ec){
    if(bytes_transferred != self->size_temp_){
      self->curr_body_ += bytes_transferred;
      self->size_temp_ -= bytes_transferred;
      self->recvBody(self->size_temp_, self);
    }else{
      self->receiveCallback();
      self->startReceive();
    }  
  } else{
    if(ec == boost::asio::error::connection_reset || ec == boost::asio::error::eof)
      self->m_socket_.close();
    fmt::print("error No. {}\terror message: {}\n", ec.value(), ec.message());
  }
}

//! @brief connection callback
//!
//! @param ec
void TCPClient::callbackConnect(const boost::system::error_code& ec){
  fmt::print("callbackConnect called\n");
  if(ec)
    fmt::print("error No. {}\terror message: {}\n", ec.value(), ec.message());
  else
  {
    fmt::print("connection finish!\n");
    conn_chk_ = true;
    startReceive();
  }
}

//! @brief async connect to the target listener
//!
//! @return true
bool TCPClient::_connect(){
  m_socket_.async_connect(*endpoint_uptr_, boost::bind(&TCPClient::callbackConnect, this, 
        boost::asio::placeholders::error));
  io_thread_ = std::thread([this](){
    m_socket_.set_option(tcp::no_delay(true));
    // m_io_service_->run();
  });
  return true;
}
