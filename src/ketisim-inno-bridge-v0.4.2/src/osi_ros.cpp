#include "osi_ros.h"

//! @brief get ros topic type from sensor type enum
//!
//! @param unsigned int idx
//!
//! @return std::string
std::string osiNros::getTopicType(int idx) {
  static const char* tmp[] = {"",
      "sensor_msgs::CameraInfo", "sensor_msgs::Image", "sensor_msgs::CompressedImage",
      "sensor_msgs::PointCloud", "sensor_msgs::PointCloud2",
      "sensor_msgs::Imu", "geometry_msgs::PoseStamped",
      "sensor_msgs::NavSatFix", "control_msg::VehicleState",
      "control_msgs::VehicleCMD", "control_msg::VehicleConfig",
      "rosgraph_msgs::Clock", "keti_msgs::PerceivedObjectArray", 
      "keti_msgs::PerceivedObjectArray"};
  return std::string(tmp[idx]);
}

//! @brief TopicInfo default constructor
osiNros::TopicInfo::TopicInfo(){
}


//! @brief default constructor with string type within member variables 
//!
//! @param index
//! @param name
//! @param type
//! @param type_str
osiNros::TopicInfo::TopicInfo(unsigned int index, std::string name, TopicType type, 
    std::string type_str) :
    idx(index),
    topic_name(name),
    topic_type(type),
    topic_type_str(type_str){
}

//! @brief default constructor with string type within member variables 
//!
//! @param index
//! @param name
//! @param type
//! @param type_str
//! @param stype
osiNros::TopicInfo::TopicInfo(unsigned int index, std::string name, TopicType type, 
    std::string type_str, SensorType stype) :
    idx(index),
    topic_name(name),
    topic_type(type),
    topic_type_str(type_str),
    sensor_type(stype){
}

//! @brief default constructor with string type within member variables
//!
//! @param index
//! @param name
//! @param type
//! @param stype
osiNros::TopicInfo::TopicInfo(unsigned int index, std::string name, TopicType type, SensorType stype) :
    idx(index),
    topic_name(name),
    topic_type(type),
    topic_type_str(getTopicType(type)),
    sensor_type(stype){
}

//! @brief default constructor with const char pointer type within member variables
//!
//! @param index
//! @param name
//! @param type
//! @param type_str
osiNros::TopicInfo::TopicInfo(unsigned int index, const char* name, TopicType type,
    const char* type_str) :
    idx(index),
    topic_name(name),
    topic_type(type),
    topic_type_str(type_str){
}

//! @brief default constructor with const char pointer type within member variables
//!
//! @param index
//! @param name
//! @param type
//! @param type_str
//! @param stype
osiNros::TopicInfo::TopicInfo(unsigned int index, const char* name, TopicType type,
    const char* type_str, SensorType stype) :
    idx(index),
    topic_name(name),
    topic_type(type),
    topic_type_str(type_str),
    sensor_type(stype){
}

//! @brief default constructor with const char pointer type within member variables 
//!
//! @param index
//! @param name
//! @param type
//! @param stype
osiNros::TopicInfo::TopicInfo(unsigned int index, const char* name, TopicType type, SensorType stype) :
    idx(index),
    topic_name(name),
    topic_type(type),
    topic_type_str(getTopicType(type)),
    sensor_type(stype){
}

//! @brief move constructor
//!
//! @param t
osiNros::TopicInfo::TopicInfo(TopicInfo &&t){
  idx = t.idx;
  topic_name = std::move(t.topic_name);
  topic_type = t.topic_type;
  topic_type_str = std::move(t.topic_type_str);
  sensor_type = t.sensor_type;
}

//! @brief copy constructor 
//!
//! @param t
osiNros::TopicInfo::TopicInfo(TopicInfo &t){
  idx = t.idx;
  topic_name = t.topic_name;
  topic_type = t.topic_type;
  topic_type_str = t.topic_type_str;
  sensor_type = t.sensor_type;
}

//! @brief copy constructor
//!
//! @param t
osiNros::TopicInfo::TopicInfo(const TopicInfo &t){
  idx = t.idx;
  topic_name = t.topic_name;
  topic_type = t.topic_type;
  topic_type_str = t.topic_type_str;
  sensor_type = t.sensor_type;
}

//! @brief default destructor
osiNros::TopicInfo::~TopicInfo(){
  topic_name.clear();
  topic_type_str.clear();
}

//! @brief copy operator
//!
//! @param t
//!
//! @return *this
osiNros::TopicInfo& osiNros::TopicInfo::operator=(const TopicInfo &t){
  idx = t.idx;
  topic_name = t.topic_name;
  topic_type = t.topic_type;
  topic_type_str = t.topic_type_str;
  sensor_type = t.sensor_type;

  return *this;
}

//! @brief copy operator
//!
//! @param t
//!
//! @return *this
osiNros::TopicInfo& osiNros::TopicInfo::operator=(TopicInfo &t){
  idx = t.idx;
  topic_name = t.topic_name;
  topic_type = t.topic_type;
  topic_type_str = t.topic_type_str;
  sensor_type = t.sensor_type;

  return *this;
}


//! @brief move operator
//!
//! @param t
//!
//! @return *this
osiNros::TopicInfo& osiNros::TopicInfo::operator=(TopicInfo &&t){
  idx = t.idx;
  topic_name = std::move(t.topic_name);
  topic_type = t.topic_type;
  topic_type_str = std::move(t.topic_type_str);
  sensor_type = t.sensor_type;

  return *this;
}

//! @brief default constructor
osiNros::TopicInfoArr::TopicInfoArr(){
}

//! @brief push back type default constructor
//!
//! @param info
osiNros::TopicInfoArr::TopicInfoArr(TopicInfo info){
  topic_info.push_back(info);
}

//! @brief copy constructor
//!
//! @param t
osiNros::TopicInfoArr::TopicInfoArr(TopicInfoArr &t) :
  topic_info(t.topic_info){
}

//! @brief const copy constructor
//!
//! @param t
osiNros::TopicInfoArr::TopicInfoArr(const TopicInfoArr &t) :
  topic_info(t.topic_info){
}

//! @brief move constructor
//!
//! @param t
osiNros::TopicInfoArr::TopicInfoArr(TopicInfoArr &&t) : 
  topic_info(std::move(t.topic_info)){
}

//! @brief default destructor
osiNros::TopicInfoArr::~TopicInfoArr(){
  topic_info.clear();
} 

//! @brief push back a copy of the TopicInfo structure
//!
//! @param info
void osiNros::TopicInfoArr::push_back(TopicInfo info){
  topic_info.push_back(info);
}

//! @brief clear topic info structures
void osiNros::TopicInfoArr::clear(){
  topic_info.clear();
}

//! @brief get size of the TopicInfo vector
//!
//! @return size of the TopicInfo vector
size_t osiNros::TopicInfoArr::size(){
  return topic_info.size();
}

//! @brief use to access with same operator
//!
//! @return first element structure 
std::vector<osiNros::TopicInfo>::iterator osiNros::TopicInfoArr::operator->(){
  if(topic_info.size() < 1){
    fmt::print(stderr,"cannot access to element!!\tvector is empty!\n");
    throw;
  }
  else
    return topic_info.begin();
}

//! @brief random access element
//!
//! @param idx
//!
//! @return target index iterator
std::vector<osiNros::TopicInfo>::iterator osiNros::TopicInfoArr::operator[](size_t idx){
  if(idx >= topic_info.size()){
    fmt::print(stderr,"cannot access to elemet!!\ttarget index is greater then size!\n");
    throw;
  }
  else
    return (topic_info.begin()+idx);
}

//! @brief move operator
//!
//! @param t
//!
//! @return *this
osiNros::TopicInfoArr& osiNros::TopicInfoArr::operator=(osiNros::TopicInfoArr &&t){
  topic_info = t.topic_info;
  return *this;
}

//! @brief defualt constructor
osiNros::SensorIdx::SensorIdx(){
}

//! @brief default constructor with id & string name value
//!
//! @param sid
//! @param sname
osiNros::SensorIdx::SensorIdx(uint64_t sid, std::string sname) :
  id(sid),
  name(sname){
}

//! @brief defualt constructor with id & const character string name value
//!
//! @param sid
//! @param sname
osiNros::SensorIdx::SensorIdx(uint64_t sid, const char* sname) :
  id(sid),
  name(sname){
}

//! @brief move constructor
//!
//! @param t
osiNros::SensorIdx::SensorIdx(SensorIdx &&t) :
  id(t.id),
  name(std::move(t.name)){
}

//! @brief parent class constructor of sensors
//!
//! @param topic_info
OsiRosDatas::OsiRosDatas(osiNros::TopicInfoArr topic_info, bool use_sim_time) :
    use_sim_time_(use_sim_time),
    topic_info_(topic_info){
  if(topic_info_.size() > 0)
    usable_ = true;
}

void OsiRosDatas::write_timestamp()
{
  if (is_write_timestamp)
    ROS_INFO_STREAM("time_Conv " << topic_info_[0]->topic_name << " : " << rostime_timestamp_s_ << "," << rostime_timestamp_ns_ << "," << timestamp_s_ << "," << timestamp_ns_);
}

//! @brief move constructor of the parent class
//!
//! @param t
OsiRosDatas::OsiRosDatas(OsiRosDatas &&t) :
  usable_(t.usable_),
  use_sim_time_(t.use_sim_time_),
  topic_info_(t.topic_info_),
  pub_(std::move(t.pub_)),
  sub_(std::move(t.sub_)),
  timestamp_s_(t.timestamp_s_),
  timestamp_ns_(t.timestamp_ns_){
}

//! @brief move operator of the parent class
//!
//! @param t
//!
//! @return *this 
OsiRosDatas& OsiRosDatas::operator=(OsiRosDatas &&t){
  usable_ = t.usable_;
  use_sim_time_ = t.use_sim_time_;
  topic_info_ = std::move(t.topic_info_);
  if(pub_.size() > 0)
    pub_ = std::move(t.pub_);
  if(sub_.size() > 0)
    sub_ = std::move(t.sub_);
  timestamp_s_ = t.timestamp_s_;
  timestamp_ns_ = t.timestamp_ns_;

  return *this;
}

//! @brief osi to ros camera class
//!
//! @param nh
//! @param topic_info
OsiRosCamera::OsiRosCamera(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time) :
    OsiRosDatas(topic_info, use_sim_time),
    camera_num_(0){
  if(usable_){
    camera_num_ = topic_info_.size()/2;
    pub_.resize(topic_info.size());
    camera_info_.resize(camera_num_);
    camera_image_.resize(camera_num_);

    for(auto &pub : pub_)
      pub = std::make_unique<ros::Publisher>();
    for(size_t i=0; i < camera_num_; ++i){
      if ( topic_info_[i*2]->topic_type == osiNros::TOPIC_COMPRESSED_IMAGE )
        *pub_[i*2] = nh->advertise<sensor_msgs::CompressedImage>(topic_info_[i*2]->topic_name,1);
      else
        *pub_[i*2] = nh->advertise<sensor_msgs::Image>(topic_info_[i*2]->topic_name,1);
      *pub_[i*2+1] = nh->advertise<sensor_msgs::CameraInfo>(topic_info_[i*2+1]->topic_name,1);
      camera_image_[i].is_bigendian = 0;
    }
  }
}


//! @brief move constructor of the ros camera class
//!
//! @param t
OsiRosCamera::OsiRosCamera(OsiRosCamera &&t) : 
  OsiRosDatas(std::move(t)),
  camera_num_(t.camera_num_),
  osi_camera_info_(t.osi_camera_info_),
  camera_info_(t.camera_info_),
  camera_image_(t.camera_image_){
}

OsiRosCamera::~OsiRosCamera(){
  camera_info_.clear();
  camera_image_.clear();
  osi_camera_info_.Clear();
}

//! @brief deserialize camera sensor information
//!
//! @param buff
//! @param osi_size
//! @param raw_size
void OsiRosCamera::deserialize(std::vector<char>::iterator buff, size_t osi_size,
    size_t raw_size){
  if(!usable_)
    return;
  osi3::SensorView camera_data;
  if(osi_size > 0)
    camera_data.ParseFromArray(buff.base(), osi_size);

  auto ros_time = ros::Time::now();
  rostime_timestamp_s_ = ros_time.sec;
  rostime_timestamp_ns_ = ros_time.nsec;

  if(camera_data.has_timestamp() && use_sim_time_){
    if(camera_data.timestamp().has_seconds())
      timestamp_s_ = camera_data.timestamp().seconds();
    if(camera_data.timestamp().has_nanos())
      timestamp_ns_ = camera_data.timestamp().nanos();
  } 
  else
  {
    timestamp_s_ = rostime_timestamp_s_;
    timestamp_ns_ = rostime_timestamp_ns_;
  }

  size_t camera_idx = 0;
  if(camera_data.has_sensor_id())
    camera_idx = camera_data.sensor_id().value();
  if((camera_idx > camera_num_) || (camera_idx == 0))
    return;
  else
    camera_idx--;
  camera_image_[camera_idx].data.clear();
  if(camera_data.camera_sensor_view_size() > 0){
    osi_camera_info_ = camera_data.camera_sensor_view(0);
    setInfo(camera_idx);
  }
  if(raw_size > 0){
    buff += osi_size;
    camera_info_[camera_idx].K.fill(0);
    camera_info_[camera_idx].K[0] = *reinterpret_cast<float*>(buff.base());
    camera_info_[camera_idx].K[4] = *reinterpret_cast<float*>((buff+4).base());
    camera_info_[camera_idx].K[2] = *reinterpret_cast<float*>((buff+8).base());
    camera_info_[camera_idx].K[5] = *reinterpret_cast<float*>((buff+12).base());
    camera_info_[camera_idx].K[8] = 1.0;
    buff += 16;

    if ( topic_info_[camera_idx*2]->topic_type == osiNros::TOPIC_COMPRESSED_IMAGE ){
      sensor_msgs::CompressedImagePtr img(new sensor_msgs::CompressedImage());
      img->header = camera_image_[camera_idx].header;
      img->format = "jpeg";
      img->data.resize(raw_size);
      std::copy(buff, (buff)+(raw_size),
              img->data.begin());

      pub_[camera_idx]->publish(img);
    }
    else {
      camera_image_[camera_idx].data.clear();
      camera_image_[camera_idx].data.resize(raw_size);
      std::copy(buff, (buff)+(raw_size),
              camera_image_[camera_idx].data.begin());
      pub_[camera_idx]->publish(camera_image_[camera_idx]);
    }
  }

  write_timestamp();
}

//! @brief set the camera information from the osi sensor data
//!
//! @param camera_idx
void OsiRosCamera::setInfo(size_t camera_idx){
  auto osi_camera_config = osi_camera_info_.view_configuration();
  
  std::string camera_name = "camera" + std::to_string(camera_idx);
  camera_info_[camera_idx].header.frame_id = camera_name;
  camera_info_[camera_idx].header.stamp = ros::Time::now();
  camera_image_[camera_idx].header = camera_info_[camera_idx].header;
  camera_info_[camera_idx].width = osi_camera_config.number_of_pixels_horizontal();
  camera_info_[camera_idx].height = osi_camera_config.number_of_pixels_vertical();
  camera_image_[camera_idx].width = osi_camera_config.number_of_pixels_horizontal();
  camera_image_[camera_idx].height = osi_camera_config.number_of_pixels_vertical();
  camera_image_[camera_idx].step = camera_image_[camera_idx].width*3;
  
  /* if you want to use the osi camera channel data, remove this commentator.
  if(osi_camera_config.channel_format_size() > 0){
    switch(osi3::CameraSensorViewConfiguration::ChannelFormat(osi_camera_config.channel_format()[0])){
      case osi3::CameraSensorViewConfiguration_ChannelFormat_CHANNEL_FORMAT_MONO_U8_LIN:
        camera_image_[camera_idx].encoding = "mono8";
        break;
      case osi3::CameraSensorViewConfiguration_ChannelFormat_CHANNEL_FORMAT_MONO_U16_LIN:
        camera_image_[camera_idx].encoding = "mono16";
        break;
      case osi3::CameraSensorViewConfiguration_ChannelFormat_CHANNEL_FORMAT_RGB_U8_LIN:
        camera_image_[camera_idx].encoding = "rgb8";
        break;
      case osi3::CameraSensorViewConfiguration_ChannelFormat_CHANNEL_FORMAT_BAYER_BGGR_U8_LIN:
        camera_image_[camera_idx].encoding = "bayer_bggr8";
        break;
      default:
        camera_image_[camera_idx].encoding = "rgb8";
        break;
    }
  } else*/
    camera_image_[camera_idx].encoding = "rgb8";
  pub_[camera_idx*2+1]->publish(camera_info_[camera_idx]);
}

//! @brief move operator
//!
//! @param t
//!
//! @return *this
OsiRosCamera& OsiRosCamera::operator=(OsiRosCamera &&t){
  OsiRosDatas::operator=(std::move(t));
  camera_num_ = t.camera_num_;
  osi_camera_info_ = t.osi_camera_info_;
  camera_info_ = std::move(t.camera_info_);
  camera_image_ = std::move(t.camera_image_);
  return *this;
}

//! @brief default constructor
//!
//! @param nh
//! @param topic_info
OsiRosLidar::OsiRosLidar(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time) :
  OsiRosDatas(topic_info, use_sim_time),
  lidar_num_(0){
  if(usable_){
    lidar_num_ = topic_info_.size();
    pub_.resize(lidar_num_);
    size_t cnt = 0;
    for(auto &pub : pub_){
      pub = std::make_unique<ros::Publisher>();
      *pub = nh->advertise<sensor_msgs::PointCloud2>(topic_info[cnt]->topic_name,1);
      ++cnt;
    }
    lidar_datas_.fields.resize(4);
    const std::array<std::string,4> tmp_str = {
      "x", "y", "z", "intensity"};
    cnt = 0;
    for(auto &t : lidar_datas_.fields){
      t.name = tmp_str[cnt];
      t.offset = cnt*4;
      t.datatype = 7;
      t.count = 1;
      ++cnt;
    }
    
    //lidar_datas_.fields[3].datatype = 8;
    lidar_datas_.is_bigendian = false;
    lidar_datas_.point_step = 16;    
    lidar_datas_.is_dense = true;
    lidar_datas_.height = 1;
  }
}

//! @brief move constructor
//!
//! @param t
OsiRosLidar::OsiRosLidar(OsiRosLidar &&t) :
  OsiRosDatas(std::move(t)),
  lidar_num_(t.lidar_num_),
  points_num_(t.points_num_),
  osi_lidar_info_(t.osi_lidar_info_),
  lidar_datas_(t.lidar_datas_){
}

//! @brief default destructor
OsiRosLidar::~OsiRosLidar(){
  osi_lidar_info_.Clear();
  lidar_datas_.fields.clear();
  lidar_datas_.data.clear();
}

//! @brief convert xyz lidar point datas to pcl2 data format
void OsiRosLidar::convertPcl2(std::vector<char>::iterator buff, size_t lidar_idx) {
  blaze::DynamicVector<float,blaze::rowVector> azimuth;
  blaze::DynamicVector<float,blaze::rowVector> elevation;
  blaze::DynamicVector<float,blaze::rowVector> distance;
  
  distance = blaze::generate<blaze::rowVector>(points_num_, [&buff](size_t idx){
      return (float)*reinterpret_cast<double*>((buff+(idx*32)+8).base());});
  azimuth = blaze::generate<blaze::rowVector>(points_num_, [&buff](size_t idx){
      return (float)*reinterpret_cast<double*>((buff+(idx*32)+16).base());});
  elevation = blaze::generate<blaze::rowVector>(points_num_, [&buff](size_t idx){
      return (float)*reinterpret_cast<double*>((buff+(idx*32)+24).base());});

  blaze::DynamicVector<float,blaze::rowVector> x_dist(distance * blaze::sin(elevation) * blaze::cos(azimuth));
  blaze::DynamicVector<float,blaze::rowVector> y_dist(distance * blaze::sin(elevation) * blaze::sin(azimuth));
  blaze::DynamicVector<float,blaze::rowVector> z_dist(distance * blaze::cos(elevation));
  lidar_datas_.data.clear();
  lidar_datas_.data.resize(16*points_num_);
  auto data_iter = lidar_datas_.data.begin();
  
  for(size_t i=0;i<points_num_;++i)
  {
    uint8_t *tmp_mem_x = reinterpret_cast<uint8_t*>((x_dist.begin()+i).base());
    uint8_t *tmp_mem_y = reinterpret_cast<uint8_t*>((y_dist.begin()+i).base());
    uint8_t *tmp_mem_z = reinterpret_cast<uint8_t*>((z_dist.begin()+i).base());
    float tmp_intensity = *reinterpret_cast<double*>((buff + (i*32)).base());
    uint8_t *tmp_inten_serial = reinterpret_cast<uint8_t*>(&tmp_intensity);
    
    if ( std::hypot(x_dist[i], y_dist[i]) < 2.5 ) continue;      

    std::memcpy((data_iter + lidar_datas_.fields[0].offset).base(), tmp_mem_x, sizeof(float));
    std::memcpy((data_iter + lidar_datas_.fields[1].offset).base(), tmp_mem_y, sizeof(float));
    std::memcpy((data_iter + lidar_datas_.fields[2].offset).base(), tmp_mem_z, sizeof(float));
    std::memcpy((data_iter + lidar_datas_.fields[3].offset).base(), tmp_inten_serial, sizeof(float));
    
    //std::memcpy((data_iter + lidar_datas_.fields[3].offset).base(), (buff + (i*32)).base(), sizeof(double));
    data_iter += 16;
  }
  lidar_datas_.header.frame_id = "velodyne";
  lidar_datas_.header.stamp.sec = timestamp_s_;
  lidar_datas_.header.stamp.nsec = timestamp_ns_;
  lidar_datas_.width = points_num_;  
  pub_[lidar_idx]->publish(lidar_datas_);
}

//! @brief deserialize lidar sensor information 
//!
//! @param buff
void OsiRosLidar::deserialize(std::vector<char>::iterator buff, size_t osi_size, size_t raw_size) 
{
  if(!usable_)
    return;

  size_t lidar_idx = 0;
  osi_lidar_info_.ParseFromArray(buff.base(), osi_size);

  auto ros_time = ros::Time::now();
  rostime_timestamp_s_ = ros_time.sec;
  rostime_timestamp_ns_ = ros_time.nsec;

  if(osi_lidar_info_.has_header()){
    if(osi_lidar_info_.header().has_measurement_time() && use_sim_time_){
      if(osi_lidar_info_.header().measurement_time().has_seconds())
        timestamp_s_ = osi_lidar_info_.header().measurement_time().seconds();
      if(osi_lidar_info_.header().measurement_time().has_nanos())
        timestamp_ns_ = osi_lidar_info_.header().measurement_time().nanos();
    }
    else{
      timestamp_s_ = rostime_timestamp_s_;
      timestamp_ns_ = rostime_timestamp_ns_;
    }
    if(osi_lidar_info_.header().has_sensor_id()){
      lidar_idx = osi_lidar_info_.header().sensor_id().value();
    }
  } 
  if((lidar_idx > lidar_num_) || (lidar_idx == 0))
    return;
  else
    lidar_idx--;
  if(raw_size > 0){
    points_num_ = raw_size/32;
    if(((raw_size%32) > 0) && ((raw_size/32) > 0))
      fmt::print(stderr,"lidar packet raw data size is unmatched!\n");
    else{
      buff += osi_size;
      convertPcl2(buff, lidar_idx);
    }
  }

  write_timestamp();
}
//! @brief move operator
//!
//! @param t
//!
//! @return *this
OsiRosLidar& OsiRosLidar::operator=(OsiRosLidar &&t){
  OsiRosDatas::operator=(std::move(t));
  lidar_num_ = t.lidar_num_;
  points_num_ = t.points_num_;
  lidar_datas_ = std::move(t.lidar_datas_);
  return *this;
}

//! @brief default constructor
//!
//! @param nh
//! @param topic_info
OsiRosRadar::OsiRosRadar(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time) : OsiRosDatas(topic_info, use_sim_time), radar_num_(0)
{ 
  if(usable_)
  {
    radar_num_ = topic_info.size();
    pub_.resize(radar_num_);
    size_t cnt=0;
    for(auto &pub : pub_){
      pub = std::make_unique<ros::Publisher>();
      *pub = nh->advertise<control_msgs::RadarData>(topic_info[cnt]->topic_name,1);
      ++cnt;
    }

    marker_publisher_ = nh->advertise<visualization_msgs::MarkerArray>("radar_markers", 0);
    sample_marker_.header.frame_id = "radar_markers";
    sample_marker_.type = 1;
    sample_marker_.action = 0;
    sample_marker_.scale = geometry_msgs::Vector3();
    sample_marker_.scale.x = 1.0;
    sample_marker_.scale.y = 1.0;
    sample_marker_.scale.z = 1.0;
    sample_marker_.color = std_msgs::ColorRGBA();
    sample_marker_.color.r = 0.0;
    sample_marker_.color.g = 1.0;
    sample_marker_.color.b = 0.0;
    sample_marker_.color.a = 1.0;
    sample_marker_.ns = "/radar_markers";
  }
}

//! @brief move constructor
//!
//! @param t
OsiRosRadar::OsiRosRadar(OsiRosRadar &&t) :
    OsiRosDatas(std::move(t)),
    radar_num_(t.radar_num_),
    radar_data_(t.radar_data_){
}

//! @brief defualt destructor
OsiRosRadar::~OsiRosRadar()
{

}

//! @brief deserialize the radar data 
//!
//! @param buff
//! @param osi_size
//! @param raw_size
void OsiRosRadar::deserialize(std::vector<char>::iterator buff, size_t osi_size, size_t raw_size){  
  if(!usable_)
    return;

  osi_radar_info_.ParseFromArray(buff.base(), osi_size);

  if (!osi_radar_info_.has_header())
  {
    std::cerr << "OsiRosRadar has no header " << std::endl;
    return;
  }

  size_t radar_idx = 0;
  if(osi_radar_info_.header().has_sensor_id())
  {
    radar_idx = osi_radar_info_.header().sensor_id().value();
  }

  if((radar_idx > radar_num_) || (radar_idx == 0))
  {
    std::cerr << "OsiRosRadar has a ID over than radar number " << std::endl;
    return;
  }
  else
  {
    radar_idx--;
  }

  auto ros_time = ros::Time::now();
  rostime_timestamp_s_ = ros_time.sec;
  rostime_timestamp_ns_ = ros_time.nsec;

  if (use_sim_time_ && osi_radar_info_.header().has_measurement_time())
  {
    if (osi_radar_info_.header().measurement_time().has_seconds() && osi_radar_info_.header().measurement_time().has_nanos())
    {
      timestamp_s_ = osi_radar_info_.header().measurement_time().seconds();
      timestamp_ns_ = osi_radar_info_.header().measurement_time().nanos();
    }
  }
  else
  {
    timestamp_s_ = rostime_timestamp_s_;
    timestamp_ns_ = rostime_timestamp_ns_;
  }

  size_t detect_count = osi_radar_info_.detection_size();
  if (is_publish_marker_ && marker_array_.markers.size() < detect_count)
    marker_array_.markers.reserve(detect_count);

  for (size_t i = 0; i < detect_count; i++)
  {    
    if ( osi_radar_info_.detection()[i].has_position()
      // && osi_radar_info_.detection()[i].has_radial_velocity()
      // osi_radar_info_.detection()[i].has_classification() &&
      // osi_radar_info_.detection()[i].has_object_id &&
      // osi_radar_info_.detection()[i].has_
    )
    {
      const osi3::Spherical3d& pos = osi_radar_info_.detection()[i].position();

      radar_data_.timestamp.sec = timestamp_s_;
      radar_data_.timestamp.nsec = timestamp_ns_;

      radar_data_.obj_status = 0;

      float x = pos.distance() * blaze::sin(pos.elevation()) * blaze::cos(pos.azimuth());
      float y = pos.distance() * blaze::sin(pos.elevation()) * blaze::sin(pos.azimuth());
      float z = pos.distance() * blaze::cos(pos.elevation());

      radar_data_.obj_lon_dist = x;
      radar_data_.obj_lat_pos = y;
      radar_data_.obj_rel_vel = static_cast<float>(osi_radar_info_.detection()[i].radial_velocity());

      pub_[radar_idx]->publish(radar_data_);

      // std::cout << x << ", " << y << ", " << z << " : " << pos.distance() << ", " << pos.elevation() << ", " << pos.azimuth() << std::endl;

      if (is_publish_marker_)
      {
        if (marker_array_.markers.size() < detect_count)
        {
          marker_array_.markers.emplace_back(visualization_msgs::Marker(sample_marker_));
        }
        
        marker_array_.markers[i].header.stamp.sec = timestamp_s_;
        marker_array_.markers[i].header.stamp.nsec = timestamp_ns_;

        marker_array_.markers[i].pose.position.x = x;
        marker_array_.markers[i].pose.position.y = y;
        marker_array_.markers[i].pose.position.z = z;
      }
    }
  }

  if (is_publish_marker_ && detect_count > 0)
    marker_publisher_.publish(marker_array_);

  write_timestamp();
}

//! @brief move operator
//!
//! @param t
//!
//! @return *this
OsiRosRadar& OsiRosRadar::operator=(OsiRosRadar &&t){
  OsiRosDatas::operator=(std::move(t));
  radar_num_ = t.radar_num_;
  radar_data_ = std::move(t.radar_data_);

  return *this;
}


//! @brief default constructor
//!
//! @param nh
//! @param topic_info
//! @param x_offset
//! @param y_offset
OsiRosVehicle::OsiRosVehicle(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time,
    double x_offset, double y_offset) :
    OsiRosDatas(topic_info, use_sim_time){
  if(usable_){
    pub_.resize(3);
    for(size_t i=0; i<3; ++i)
      pub_[i] = std::make_unique<ros::Publisher>();
    *pub_[0] = nh->advertise<geometry_msgs::PoseStamped>(topic_info[0]->topic_name,1);
    *pub_[1] = nh->advertise<nav_msgs::Odometry>(topic_info[1]->topic_name,1);
    *pub_[2] = nh->advertise<control_msgs::VehicleState>("/vehicle_state",1);

    pose_offset_[0] = x_offset;
    pose_offset_[1] = y_offset;
  }
}

//! @brief move constructor
//!
//! @param t
OsiRosVehicle::OsiRosVehicle(OsiRosVehicle &&t) :
  OsiRosDatas(std::move(t)),
  pose_datas_(t.pose_datas_),
  odom_datas_(t.odom_datas_),
  vehicle_state_datas_(t.vehicle_state_datas_){
}

//! @brief default destructor
OsiRosVehicle::~OsiRosVehicle(){
}


//! @brief deserialize the pose and orientation datas
//!
//! @param buff
void OsiRosVehicle::callback(std::vector<char>::iterator buff, GpsType gps_type){
  if(!usable_)
    return;

  vehicle_state_.deserialize(buff);
  
  if(use_sim_time_){
    vehicle_state_datas_.header.stamp.sec = vehicle_state_.measure_time.time_sec; 
    vehicle_state_datas_.header.stamp.nsec = vehicle_state_.measure_time.time_nsec;
    pose_datas_.header.stamp.sec = vehicle_state_.measure_time.time_sec;
    pose_datas_.header.stamp.nsec = vehicle_state_.measure_time.time_nsec;
    odom_datas_.header.stamp.sec = vehicle_state_.measure_time.time_sec;
    odom_datas_.header.stamp.nsec = vehicle_state_.measure_time.time_nsec;
  } else{
    vehicle_state_datas_.header.stamp = ros::Time::now();
    pose_datas_.header.stamp = ros::Time::now();
    odom_datas_.header.stamp = ros::Time::now();
  }
  
  // vehicle_state_.gps_data.lat_x -= pose_offset_[0];
  // vehicle_state_.gps_data.lon_y -= pose_offset_[1];

  if(gps_type == GPS_UTM){

    
    vehicle_state_datas_.x = vehicle_state_.gps_data.lat_x;
    vehicle_state_datas_.y = vehicle_state_.gps_data.lon_y;

    pose_datas_.pose.position.x = vehicle_state_.gps_data.lat_x;
    pose_datas_.pose.position.y = vehicle_state_.gps_data.lon_y;
  }
  else
  {
    // todo :: something calc to transform the WGS datas to UTM datas
    vehicle_state_datas_.x = vehicle_state_.gps_data.lat_x;
    vehicle_state_datas_.y = vehicle_state_.gps_data.lon_y;

    pose_datas_.pose.position.x = vehicle_state_.gps_data.lat_x;
    pose_datas_.pose.position.y = vehicle_state_.gps_data.lon_y;
  }

  vehicle_state_datas_.yaw_rate = vehicle_state_.imu_data.gyro[2];
  vehicle_state_datas_.a_x = vehicle_state_.imu_data.accel[0];
  vehicle_state_datas_.heading = vehicle_state_.imu_data.heading;
  
  pose_datas_.header.frame_id = "map";
  
  // pose_datas_.pose.orientation = tf::createQuaternionMsgFromYaw((vehicle_state_.imu_data.heading+90) * M_PI / 180.0);

  // TODO(youngbo) : Debuging adSim_v0.7
  pose_datas_.pose.orientation = tf::createQuaternionMsgFromYaw(vehicle_state_.imu_data.heading * M_PI / 180.0);

  // TODO(youngbo) : Debuging adSim_v0.7.2(Daegu, not Sangam)
  // pose_datas_.pose.orientation = tf::createQuaternionMsgFromYaw((vehicle_state_.imu_data.heading+180) * M_PI / 180.0);


  tf::Quaternion q(
    pose_datas_.pose.orientation.x,
    pose_datas_.pose.orientation.y,
    pose_datas_.pose.orientation.z,
    pose_datas_.pose.orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);  
  vehicle_state_datas_.heading = yaw;
  
  // pose_datas_.pose.orientation = tf::createQuaternionMsgFromYaw(
  //     vehicle_state_.imu_data.gyro[2]);

  odom_datas_.header.frame_id = "map";
  odom_datas_.pose.pose = pose_datas_.pose;
  odom_datas_.pose.pose.position.x -= pose_offset_[0];
  odom_datas_.pose.pose.position.y -= pose_offset_[1];

  // TODO(youngbo) : Debuging adSim_v0.7.2(Daegu, not Sangam)
  // odom_datas_.pose.pose.position.x = -odom_datas_.pose.pose.position.x;
  // odom_datas_.pose.pose.position.y = -odom_datas_.pose.pose.position.y;

  vehicle_state_datas_.x = odom_datas_.pose.pose.position.x+pose_offset_[0];
  vehicle_state_datas_.y = odom_datas_.pose.pose.position.y+pose_offset_[1];
  pose_datas_.pose.position.x = odom_datas_.pose.pose.position.x+pose_offset_[0];
  pose_datas_.pose.position.y = odom_datas_.pose.pose.position.y+pose_offset_[1];

  geometry_msgs::TransformStamped odom_trans;
  odom_trans.header.stamp = ros::Time::now();
  odom_trans.header.frame_id = "map";
  odom_trans.child_frame_id = "base_link";

  odom_trans.transform.translation.x = odom_datas_.pose.pose.position.x;
  odom_trans.transform.translation.y = odom_datas_.pose.pose.position.y;
  odom_trans.transform.translation.z = odom_datas_.pose.pose.position.z;
  odom_trans.transform.rotation = odom_datas_.pose.pose.orientation;

  //send the transform
  static tf::TransformBroadcaster odom_broadcaster;
  odom_broadcaster.sendTransform(odom_trans);

  pub_[0]->publish(pose_datas_);
  pub_[1]->publish(odom_datas_);

  switch(vehicle_state_.driving_mode_state){
    case DrivingMode::DRIVING_NONE_EXCEPTION:
      vehicle_state_datas_.autonomous_status = 0x00;
    case DrivingMode::DRIVING_MANUAL:
      vehicle_state_datas_.autonomous_status = 0x01;
      break;
    case DrivingMode::DRIVING_FULL_AUTO:
      vehicle_state_datas_.autonomous_status = 0x02;
      break;
    case DrivingMode::DRIVING_CC_AUTO:
      vehicle_state_datas_.autonomous_status = 0x03;
      break;
    case DrivingMode::DRIVING_STEER_AUTO:
      vehicle_state_datas_.autonomous_status = 0x04;
      break;
  }
  switch(vehicle_state_.blinker_state){
    case BlinkerState::BLINKER_NONE_EXCEPTION:
      vehicle_state_datas_.turn_signal = 0x0;
      break;
    case BlinkerState::BLINKER_LEFT:
      vehicle_state_datas_.turn_signal = 0x04;
      break;
    case BlinkerState::BLINKER_FLASHER:
      vehicle_state_datas_.turn_signal = 0x02;
      break;
    case BlinkerState::BLINKER_RIGHT:
      vehicle_state_datas_.turn_signal = 0x01;
      break;
  }
  switch(vehicle_state_.gear_state){
    case GearState::GEAR_NONE_EXCEPTION:
      fmt::print(stderr, "current ego vehicle's gear state is none exception!!\nset to parking.\n");
      vehicle_state_datas_.gear_sel = 0x00;
      break;
    case GearState::GEAR_PARKING:
      vehicle_state_datas_.gear_sel = 0x00;
      break;
    case GearState::GEAR_REVERSE:
      vehicle_state_datas_.gear_sel = 0x07;
      break;
    case GearState::GEAR_NEUTRAL:
      vehicle_state_datas_.gear_sel = 0x06;
      break;
    case GearState::GEAR_DRIVING:
      vehicle_state_datas_.gear_sel = 0x05;
      break;
  }
  vehicle_state_datas_.brake_pedal = vehicle_state_.break_state;
  vehicle_state_datas_.v_ego = vehicle_state_.speed*3.6; // m/s to km/h
  vehicle_state_datas_.steer_angle = vehicle_state_.steer * 180 / M_PI * STEER_RATIO;  
  
  pub_[2]->publish(vehicle_state_datas_);
}

//! @brief move operator
//!
//! @param t
//!
//! @return *this 
OsiRosVehicle& OsiRosVehicle::operator=(OsiRosVehicle &&t){
  OsiRosDatas::operator=(std::move(t));
  pose_datas_ = t.pose_datas_;
  odom_datas_ = t.odom_datas_;
  vehicle_state_datas_ = t.vehicle_state_datas_;

  return *this;
}

//! @brief default constructor
//!
//! @param nh
//! @param topic_info
OsiRosControl::OsiRosControl(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time) :
    OsiRosDatas(topic_info, use_sim_time){
  if(usable_){
    sub_.resize(topic_info.size());
    for(auto &sub : sub_)
      sub = std::make_unique<ros::Subscriber>();

    *sub_[0] = nh->subscribe(topic_info[0]->topic_name, 1, 
        &OsiRosControl::callbackControl, this);
    *sub_[1] = nh->subscribe(topic_info[1]->topic_name, 1, 
        &OsiRosControl::callbackConfig, this);

    control_datas_.gear_state = GearState::GEAR_DRIVING;
    control_datas_.blinker_state = BlinkerState::BLINKER_NONE_EXCEPTION;
  }
}

//! @brief move constructor
//!
//! @param t
OsiRosControl::OsiRosControl(OsiRosControl &&t) :
  OsiRosDatas(std::move(t)),
  control_datas_(std::move(t.control_datas_)),
  send_(std::move(t.send_)){
}

//! @brief destructor
OsiRosControl::~OsiRosControl(){
}

//! @brief ros vehicle command topic callback
//!
//! @param msg
void OsiRosControl::callbackControl(const control_msgs::VehicleCMDConstPtr &msg){
  if(!usable_)
    return;

  SimulatorTimeTick timeTick;
  timeTick.time_sec = msg->header.stamp.sec;
  timeTick.time_nsec = msg->header.stamp.nsec;

  control_datas_.measure_time = timeTick;
  control_datas_.input_pedal_accel = msg->accel_decel_cmd;  
  control_datas_.steer = -msg->steer_angle_cmd;

  // control_datas_.gear_state = GEAR_REVERSE;
  // control_datas_.driving_mode_state = DRIVING_FULL_AUTO;
  // control_datas_.steer = msg->steer_angle_cmd;

  auto datas = serialize();

  // std::cout << "origin steer : " << msg->steer_angle_cmd << std::endl;
  // std::cout << "steer : " << con_data.steer << std::endl;
  // std::cout << "accel : " << con_data.input_pedal_accel << std::endl;

  if(send_)
    send_(datas);
}

void OsiRosControl::callbackConfig(const control_msgs::VehicleConfigConstPtr &msg){
  if(!usable_)
    return;
  
  switch(msg->turn_signal_cmd){
    case 0x01:
      control_datas_.blinker_state = BlinkerState::BLINKER_RIGHT;
      break;
    case 0x02:
      control_datas_.blinker_state = BlinkerState::BLINKER_FLASHER;
      break;
    case 0x04:
      control_datas_.blinker_state = BlinkerState::BLINKER_LEFT;
      break;
    default:
      control_datas_.blinker_state = BlinkerState::BLINKER_NONE_EXCEPTION;
      break;
  }
}

//! @brief set the network data send function
//!
//! @param func
void OsiRosControl::setSendFunction(std::function<bool (std::vector<char>)> func){
  send_ = func;
}

//! @brief serialize the control data
//!
//! @return tmp 
std::vector<char> OsiRosControl::serialize(){
  std::vector<char> tmp_buff = {0x48, 0x53, 0x57, 0x02};
  tmp_buff[3] = DataClassFlag::FLAG_CONTROL_INPUT;
  tmp_buff.resize(12);

  size_t serial_size = control_datas_.serialize(tmp_buff);

  serialSizeApply(serial_size, tmp_buff);
  
  return tmp_buff;
}

//! @brief move operator
//!
//! @param t
//!
//! @return *this
OsiRosControl& OsiRosControl::operator=(OsiRosControl &&t){
  OsiRosDatas::operator=(std::move(t));
  control_datas_ = std::move(t.control_datas_);
  send_ = std::move(t.send_);

  return *this;
}

//! @brief default constructor
//!
//! @param nh
//! @param topic_info
OsiRosTimer::OsiRosTimer(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time) :
    OsiRosDatas(topic_info, use_sim_time),
    current_sec(0),
    current_nsec(0){
  pub_.resize(1);
  pub_[0] = std::make_unique<ros::Publisher>();
  *pub_[0] = nh->advertise<rosgraph_msgs::Clock>("/clock",1);
}

//! @brief move constructor
//!
//! @param t
OsiRosTimer::OsiRosTimer(OsiRosTimer &&t) :
    OsiRosDatas(std::move(t)),
    current_sec(t.current_sec),
    current_nsec(t.current_nsec){
}

//! @brief default destructor
OsiRosTimer::~OsiRosTimer(){
}

//! @brief update simulation time & publish
//!
//! @param csec
//! @param cnsec
void OsiRosTimer::update(uint64_t csec, uint64_t cnsec){
  current_sec = csec;
  current_nsec = cnsec;

  rosgraph_msgs::Clock current_time;
  current_time.clock.nsec = current_nsec;
  current_time.clock.sec = current_sec;

  pub_[0]->publish(current_time);
}

OsiRosGT::OsiRosGT(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time)
 : OsiRosDatas(topic_info, use_sim_time)
 {
  pub_.resize(1);
  pub_[0] = std::make_unique<ros::Publisher>();
  *pub_[0] = nh->advertise<keti_msgs::PerceivedObjectArray>(topic_info_->topic_name, 1);

  marker_publisher_ = nh->advertise<visualization_msgs::MarkerArray>("gt_markers", 0);
  pub_gt_to_mobileye_ = nh->advertise<mobileye_560_660_msgs::ObstacleData>("/parsed_tx/obstacle_data", 0);

  sample_marker_.header.frame_id = "/base_link";  
  sample_marker_.type = visualization_msgs::Marker::CUBE;;
  sample_marker_.action = visualization_msgs::Marker::ADD;

  sample_marker_.scale = geometry_msgs::Vector3();
  sample_marker_.scale.x = 4.65;
  sample_marker_.scale.y = 1.6;
  sample_marker_.scale.z = 1.825;

  sample_marker_.color = std_msgs::ColorRGBA();
  sample_marker_.color.r = 1.0;
  sample_marker_.color.g = 0.8;
  sample_marker_.color.b = 0.36;
  sample_marker_.color.a = 0.6;
  
  sample_marker_.ns = "/gt_markers";
}

OsiRosGT::OsiRosGT(OsiRosGT &&t) :
    OsiRosDatas(std::move(t)){
}

OsiRosGT::~OsiRosGT(){
}

void OsiRosGT::deserialize(std::vector<char>::iterator buff, size_t osi_size, size_t raw_size){
  if(osi_size < 1)
    return;

  osi_gt_datas_.ParseFromArray(buff.base(), osi_size);
  size_t gt_obj_size = osi_gt_datas_.moving_object_size();
  gt_datas_.objects.clear();

  if(gt_obj_size < 1)
    return;

  auto ros_time = ros::Time::now();
  rostime_timestamp_s_ = ros_time.sec;
  rostime_timestamp_ns_ = ros_time.nsec;

  if (osi_gt_datas_.has_timestamp() && use_sim_time_)
  {
    if (osi_gt_datas_.timestamp().has_seconds() && osi_gt_datas_.timestamp().has_nanos())
    {
      timestamp_s_ = osi_gt_datas_.timestamp().seconds();
      timestamp_ns_ = osi_gt_datas_.timestamp().nanos();
    }
  }
  else
  {
    timestamp_s_ = rostime_timestamp_s_;
    timestamp_ns_ = rostime_timestamp_ns_;
  }

  gt_datas_.objects.resize(gt_obj_size);
  if (is_publish_marker_ && marker_array_.markers.size() < gt_obj_size)
    marker_array_.markers.reserve(gt_obj_size);

  auto deg2rad = [](double angle)->double { return angle*M_PI/180.; };      

  for(size_t i=0; i < gt_obj_size; ++i)
  {
    auto obj = osi_gt_datas_.moving_object(i);
    auto target = gt_datas_.objects.begin() + i;

    if(obj.has_header())
    {
      target->id = obj.header().tracking_id().value();
    }
    else
    {
      return;
    }
    
    if(obj.has_base()){

      if(obj.base().has_position()){
        target->pose.position.x = obj.base().position().x();
        target->pose.position.y = obj.base().position().y();
        target->pose.position.z = 0.5;        
      } 
      else
      {
        return;
      }

      if(obj.base().has_orientation()){
        auto ori = obj.base().orientation();
        target->pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(deg2rad(ori.roll()),                                                                          
                                                                          deg2rad(ori.pitch()),
                                                                          deg2rad(ori.yaw()));
      } 
      else
      {
        return;
      }

      if(obj.base().has_velocity()){
        auto vel = obj.base().velocity();
        target->velocity.linear.x = vel.x();
        target->velocity.linear.y = vel.y();
        target->velocity.linear.z = vel.z();
      } 
      else
      {
        return;
      }

      if(obj.base().has_dimension()){
        auto dim = obj.base().dimension();
        target->dimensions.x = dim.length();
        target->dimensions.y = dim.width();
        target->dimensions.z = dim.height();
      } 
      else
      {
        return;
      }
    }

    if(obj.candidate_size() > 0){
      if(obj.candidate(0).has_type()){
        int type = obj.candidate(0).type();
        target->label = labelTyper(type);
      }
    } else
    {
      return;
    }

    // target->valid = true;
    // target->pose_reliable = true;
    target->header.frame_id = "/base_link";

    if (is_publish_marker_)
    {
      if (marker_array_.markers.size() < gt_obj_size)
      {
        marker_array_.markers.emplace_back(visualization_msgs::Marker(sample_marker_));
      }
      
      marker_array_.markers[i].header.stamp.sec = timestamp_s_;
      marker_array_.markers[i].header.stamp.nsec = timestamp_ns_;
      marker_array_.markers[i].pose.position.x = obj.base().position().x();
      marker_array_.markers[i].pose.position.y = obj.base().position().y();
      marker_array_.markers[i].pose.position.z = obj.base().position().z();
      marker_array_.markers[i].pose.orientation = tf::createQuaternionMsgFromRollPitchYaw
      (
        obj.base().orientation().roll() * (M_PI / 180),
        obj.base().orientation().pitch() * (M_PI / 180),
        obj.base().orientation().yaw() * (M_PI / 180)
      );
    }
  }

  gt_datas_.header.frame_id = "/base_link";
  pub_[0]->publish(gt_datas_);

  // TODO
  mobileye_560_660_msgs::ObstacleData obstacles;
  obstacles.header.stamp.sec = timestamp_s_;
  obstacles.header.stamp.nsec = timestamp_ns_;

  if ( gt_datas_.objects.size() == 0 ){
    obstacles.obstacle_status = 0;
    obstacles.obstacle_pos_x = 150.;
    obstacles.obstacle_pos_y = 0.0;
    obstacles.obstacle_rel_vel_x = 0.0;
  }
  else {
    for ( auto& obj: gt_datas_.objects ){      
      if ( obj.pose.position.x > 0 && fabs(obj.pose.position.y) < 10.0 ){          
        obstacles.obstacle_status = 1;
        obstacles.obstacle_pos_x = obj.pose.position.x - 2.0;
        obstacles.obstacle_pos_y = obj.pose.position.y;
        obstacles.obstacle_rel_vel_x = obj.velocity.linear.x;

        pub_gt_to_mobileye_.publish(obstacles);
        sleep(0.001);
      }
    }    
  }  

  if (is_publish_marker_ && gt_obj_size > 0)
    marker_publisher_.publish(marker_array_);

  write_timestamp();
}

std::string OsiRosGT::labelTyper(int type) {
  static const char* labels[] = {"Unknown", "Other", "Vehicle", "Pedestrian", "Animal"};
  std::string tmp;
  if(0 < type && type < 5)
    tmp = std::string(labels[type]);
  else
    tmp = std::string(labels[0]);

  return tmp;
}