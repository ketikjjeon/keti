/**
 * @file osi_ros.h
 *
 * @brief convert packets between open simulation interface data 
 * and ros kinetic data.
 *
 * @author Sangwook Ha  swid1@keti.re.kr
 *
 **/

#ifndef OSI_N_ROS_H_
#define OSI_N_ROS_H_

#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdint>
#include <cstring>
#include <fstream>
#include <sstream>

#include <fmt/core.h>
#include <fmt/format.h>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <blaze/Blaze.h>
#include <blaze/Math.h>
#include <blaze/math/DynamicVector.h>
#include <blaze/Util.h>

#include "keti_interface.h"
#include <osi3/osi_common.pb.h>
#include <osi3/osi_datarecording.pb.h>
#include <osi3/osi_detectedobject.pb.h>
#include <osi3/osi_sensordata.pb.h>
#include <osi3/osi_sensorview.pb.h>
#include <osi3/osi_featuredata.pb.h>
#include <osi3/osi_version.pb.h>

#include <ros/ros.h>
#include <ros/package.h>
#include <rosgraph_msgs/Clock.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <tf/LinearMath/Vector3.h>
#include <tf/LinearMath/Quaternion.h>
#include <tf/LinearMath/Transform.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <control_msgs/VehicleCMD.h>
#include <control_msgs/RadarData.h>
#include <control_msgs/VehicleState.h>
#include <control_msgs/VehicleConfig.h>
#include <keti_msgs/PerceivedObject.h>
#include <keti_msgs/PerceivedObjectArray.h>
#include <visualization_msgs/MarkerArray.h>
#include <mobileye_560_660_msgs/ObstacleData.h>

#include "network_client.h"

#define STEER_RATIO   17.0

using RosNodeHandleSptr = std::shared_ptr<ros::NodeHandle>;

namespace osiNros{

std::string getTopicType(int idx);

//! @brief ros topic types
enum TopicType : int {
  TOPIC_CAMERA_INFO = 1,
  TOPIC_RAW_IMAGE,
  TOPIC_COMPRESSED_IMAGE,
  TOPIC_POINTCLOUD,
  TOPIC_POINTCLOUD2,
  TOPIC_IMU,
  TOPIC_POSITION,
  TOPIC_ODOMETRY,
  TOPIC_VEHICLE_STATE,
  TOPIC_CONTROL,
  TOPIC_CONTROL_CONFIG,
  TOPIC_TIME,
  TOPIC_GT,
  TOPIC_RADAR,
};

//! @brief osi sensor types
enum SensorType : int {
  SENSOR_CAMERA = 1,
  SENSOR_LIDAR,
  SENSOR_GT,
  SENSOR_RADAR,
  SENSOR_ULTRASONIC,
  SENSOR_GNSS_IMU,
};

//! @brief basic ros topic information
typedef struct TopicInfo{
  uint64_t idx;
  std::string topic_name;
  TopicType topic_type;
  std::string topic_type_str;
  SensorType sensor_type;
  
  TopicInfo();
  TopicInfo(unsigned int index, std::string name, TopicType type, std::string type_str);
  TopicInfo(unsigned int index, std::string name, TopicType type, std::string type_str, SensorType stype);  
  TopicInfo(unsigned int index, std::string name, TopicType type, SensorType stype);
  TopicInfo(unsigned int index, std::string name, SensorType stype);
  TopicInfo(unsigned int index, const char* name, TopicType type, const char* type_str);
  TopicInfo(unsigned int index, const char* name, TopicType type, const char* type_str, SensorType stype);  
  TopicInfo(unsigned int index, const char* name, TopicType type, SensorType stype);
  TopicInfo(unsigned int index, const char* name, SensorType stype);

  TopicInfo(TopicInfo &&t);
  TopicInfo(TopicInfo &t);
  TopicInfo(const TopicInfo &t);

  ~TopicInfo();

  TopicInfo& operator=(TopicInfo &t);
  TopicInfo& operator=(const TopicInfo &t);
  TopicInfo& operator=(TopicInfo &&t);
  
} TopicInfo;

typedef struct TopicInfoArr{
  std::vector<TopicInfo> topic_info;

  TopicInfoArr();
  TopicInfoArr(TopicInfo info);
  TopicInfoArr(TopicInfoArr &t);
  TopicInfoArr(const TopicInfoArr &t);
  TopicInfoArr(TopicInfoArr &&t);
  ~TopicInfoArr();

  void push_back(TopicInfo info);

  //! @brief emplace back to TopicInfo vector
  //!
  //! @tparam typename
  //! @tparam Args
  //! @param idx
  //! @param args
  template<typename=unsigned int, typename... Args>
  void emplace_back(unsigned int idx, Args... args){
    topic_info.emplace_back(idx, args...);
  }

  void clear();

  size_t size();

  std::vector<TopicInfo>::iterator operator->();
  std::vector<TopicInfo>::iterator operator[](size_t idx);
  TopicInfoArr& operator=(TopicInfoArr &&t);

} TopicInfoArr;

//! @brief matching osi sensor index number to ros topic name
typedef struct SensorIdx {
  uint64_t id;
  std::string name;

  SensorIdx();
  SensorIdx(uint64_t sid, std::string sname);
  SensorIdx(uint64_t sid, const char* sname);
  SensorIdx(SensorIdx &&t);
  ~SensorIdx();

} SensorIdx;
}
//! @brief the basic class which convert packets between open simulator interface 
//!  and ros message.
class OsiRosDatas{
  public:
    OsiRosDatas(osiNros::TopicInfoArr topic_info, bool use_sim_time);
    OsiRosDatas(OsiRosDatas &&t);
    virtual ~OsiRosDatas(){
      topic_info_.clear();
      if(pub_.size() > 0)
        pub_.clear();
      if(sub_.size() > 0)
        sub_.clear();
    }

    OsiRosDatas& operator=(OsiRosDatas &&t);

    bool is_write_timestamp = false;
    void write_timestamp();

    bool usable_ = false;
    bool use_sim_time_ = false;
    osiNros::TopicInfoArr topic_info_;
    std::vector<std::unique_ptr<ros::Publisher>> pub_;
    std::vector<std::unique_ptr<ros::Subscriber>> sub_;
    uint32_t timestamp_s_ = 0;
    uint32_t timestamp_ns_ = 0;

    uint32_t rostime_timestamp_s_ = 0;
    uint32_t rostime_timestamp_ns_ = 0;
};

//! @brief convert camera datas from osi to ros. 
class OsiRosCamera : public OsiRosDatas {
  public:
    OsiRosCamera(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time);
    OsiRosCamera(OsiRosCamera &&t);
    ~OsiRosCamera();
    
    void deserialize(std::vector<char>::iterator buff, size_t osi_size,
        size_t raw_size);
    void setInfo(size_t camera_idx);
    OsiRosCamera& operator=(OsiRosCamera &&t);

  private:
    size_t camera_num_;
    ::osi3::CameraSensorView osi_camera_info_;
    std::vector<sensor_msgs::CameraInfo> camera_info_;
    std::vector<sensor_msgs::Image> camera_image_;    
};

//! @brief convert lidar datas from osi to ros.
class OsiRosLidar : public OsiRosDatas {
  public:
    OsiRosLidar(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time);
    OsiRosLidar(OsiRosLidar &&t);
    ~OsiRosLidar();
    

    void convertPcl2(std::vector<char>::iterator buff, size_t lidar_idx);
    void deserialize(std::vector<char>::iterator buff, size_t osi_size,
        size_t raw_size);


    OsiRosLidar& operator=(OsiRosLidar &&t);
  private:
    size_t lidar_num_;
    size_t points_num_;
    ::osi3::LidarDetectionData osi_lidar_info_;
    sensor_msgs::PointCloud2 lidar_datas_;
    
};

//! @brief convert radar datas from osi to ros.
class OsiRosRadar : public OsiRosDatas{
  public:
    OsiRosRadar(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time);
    OsiRosRadar(OsiRosRadar &&t);
    ~OsiRosRadar();

    void deserialize(std::vector<char>::iterator buff, size_t osi_size,
        size_t raw_size);

    OsiRosRadar& operator=(OsiRosRadar &&t);

  private:
    size_t radar_num_;
    ::osi3::RadarDetectionData osi_radar_info_;
    control_msgs::RadarData radar_data_;
    
    bool is_publish_marker_ = true;
    visualization_msgs::MarkerArray marker_array_;
    visualization_msgs::Marker sample_marker_;
    ros::Publisher marker_publisher_;
};

//! @brief convert pose and vehicle status datas from osi to ros.
class OsiRosVehicle : public OsiRosDatas {
  public:
    OsiRosVehicle(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info_arr, bool use_sim_time,
        double x_offset, double y_offset);
    OsiRosVehicle(OsiRosVehicle &&t);
    ~OsiRosVehicle();
    
    void callback(std::vector<char>::iterator buff, GpsType gps_type);

    OsiRosVehicle& operator=(OsiRosVehicle &&t);
  private:
    SimulationVehicleState vehicle_state_;
    geometry_msgs::PoseStamped pose_datas_;
    nav_msgs::Odometry odom_datas_;
    control_msgs::VehicleState vehicle_state_datas_;
    double pose_offset_[2];
};

//! @brief convert control datas from ros to keti fbs.
class OsiRosControl : public OsiRosDatas {
  public:
    OsiRosControl(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time);
    OsiRosControl(OsiRosControl &&t);
    ~OsiRosControl();
    
    void callbackControl(const control_msgs::VehicleCMDConstPtr &msg);
    void callbackConfig(const control_msgs::VehicleConfigConstPtr &msg);
    void setSendFunction(std::function<bool (std::vector<char>)> func);
    std::vector<char> serialize();

    OsiRosControl& operator=(OsiRosControl &&t);
  private:
    SimulationControlData control_datas_;
    std::function <bool (std::vector<char>)> send_;
};

class OsiRosTimer : public OsiRosDatas {
  public:
    OsiRosTimer(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time);
    OsiRosTimer(OsiRosTimer &&t);
    ~OsiRosTimer();

    void update(uint64_t csec, uint64_t cnsec);

  private:
    uint64_t current_sec;
    uint64_t current_nsec;
};

class OsiRosGT : public OsiRosDatas {
  public:
    OsiRosGT(RosNodeHandleSptr nh, osiNros::TopicInfoArr topic_info, bool use_sim_time);
    OsiRosGT(OsiRosGT &&t);
    ~OsiRosGT();

    void deserialize(std::vector<char>::iterator buff, size_t osi_size,
        size_t raw_size);

  private:
    osi3::SensorData osi_gt_datas_;
    keti_msgs::PerceivedObjectArray gt_datas_;

    bool is_publish_marker_ = true;
    visualization_msgs::MarkerArray marker_array_;
    visualization_msgs::Marker sample_marker_;
    ros::Publisher marker_publisher_;

    // TODO(youngbo) : Temporarily, GT data is used as a radar.
    ros::Publisher pub_gt_to_mobileye_;

    std::string labelTyper(int type);
};


#endif

