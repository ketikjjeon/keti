/**
 * @file network_client.h
 *
 * @brief This header include ethernet TCP communication based
 *  endpoint client class.
 *
 * @author Sangwook Ha
 *  swid1@keti.re.kr
 *
 */

#ifndef NETWORK_CLIENT_H_
#define NETWORK_CLIENT_H_

#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <iterator>
#include <thread>
#include <deque>
#include <string>
#include <cstring>

#include <boost/asio.hpp>
#include <boost/ref.hpp>
#include <boost/system/error_code.hpp>
#include <boost/bind.hpp>
#include <boost/version.hpp>

#include <fmt/core.h>

using boost::asio::io_service;
using boost::asio::ip::tcp;

typedef std::deque< std::vector<char> > PacketQueue;
typedef std::vector<char> RecvPacket;

typedef std::shared_ptr<io_service> io_service_sptr;

std::string hexStr(char *data, int len);

//! @brief TCP client basic class
class TCPClient: public std::enable_shared_from_this<TCPClient>{
  public:
    TCPClient(io_service_sptr io_service, size_t header_size);
    TCPClient(TCPClient &&t);
    ~TCPClient();
    

    bool _connect(const std::string &ip, const std::string &port);
    bool _connect(std::string &ip, int port);
    void _disconnect();
    bool isConnected();
    bool _send(std::vector<char> &&data);
        
    TCPClient& operator=(TCPClient &&t);

    RecvPacket m_receive_header_;
    RecvPacket m_receive_body_;
    RecvPacket receive_header_checker_;
    uint32_t curr_body_;
  // private:
    size_t header_size_;
    size_t recv_buff_idx_ = 0;
    size_t size_temp_ = 0;
    std::thread io_thread_;
    io_service_sptr m_io_service_;
    tcp::socket m_socket_;
    PacketQueue send_packet_buff_ ;
    std::unique_ptr<tcp::endpoint> endpoint_uptr_;
    bool conn_chk_ = false;

    virtual void receiveCallback() = 0;
    virtual size_t headerChecker() = 0;

    void dataSend();
    void startReceive();
    void startReceive(size_t head_size);
    void recvBody(size_t recv_size, std::shared_ptr<TCPClient>);
    void HandleReceive(const boost::system::error_code& ec, size_t bytes_transferred, std::shared_ptr<TCPClient>);
    void handleBody(const boost::system::error_code& ec, size_t bytes_transferred, std::shared_ptr<TCPClient>);
    void callbackConnect(const boost::system::error_code& ec);
    bool _connect();
};

#endif
