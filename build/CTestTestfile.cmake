# CMake generated Testfile for 
# Source directory: /home/keti/ketisim_interface/src
# Build directory: /home/keti/ketisim_interface/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(ketisim-inno-bridge-v0.4.2)
subdirs(ketisim-inno-bridge-daegu)
