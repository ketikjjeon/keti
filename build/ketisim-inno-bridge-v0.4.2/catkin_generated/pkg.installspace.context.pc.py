# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;roslib;rosgraph_msgs;std_msgs;sensor_msgs;tf;geometry_msgs;nav_msgs;control_msgs;visualization_msgs;keti_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "ketisim_inno_bridge"
PROJECT_SPACE_DIR = "/home/keti/ketisim_interface/install"
PROJECT_VERSION = "0.3.0"
