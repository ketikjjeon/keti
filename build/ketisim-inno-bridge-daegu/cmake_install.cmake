# Install script for directory: /home/keti/ketisim_interface/src/ketisim-inno-bridge-daegu

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/keti/ketisim_interface/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/keti/ketisim_interface/build/ketisim-inno-bridge-daegu/catkin_generated/installspace/ketisim_inno_bridge_daegu.pc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/ketisim_inno_bridge_daegu/cmake" TYPE FILE FILES
    "/home/keti/ketisim_interface/build/ketisim-inno-bridge-daegu/catkin_generated/installspace/ketisim_inno_bridge_daeguConfig.cmake"
    "/home/keti/ketisim_interface/build/ketisim-inno-bridge-daegu/catkin_generated/installspace/ketisim_inno_bridge_daeguConfig-version.cmake"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/ketisim_inno_bridge_daegu" TYPE FILE FILES "/home/keti/ketisim_interface/src/ketisim-inno-bridge-daegu/package.xml")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/ketisim_inno_bridge_daegu/ketisim_inno_bridge_daegu" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/ketisim_inno_bridge_daegu/ketisim_inno_bridge_daegu")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/ketisim_inno_bridge_daegu/ketisim_inno_bridge_daegu"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/ketisim_inno_bridge_daegu" TYPE EXECUTABLE FILES "/home/keti/ketisim_interface/devel/lib/ketisim_inno_bridge_daegu/ketisim_inno_bridge_daegu")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/ketisim_inno_bridge_daegu/ketisim_inno_bridge_daegu" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/ketisim_inno_bridge_daegu/ketisim_inno_bridge_daegu")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/ketisim_inno_bridge_daegu/ketisim_inno_bridge_daegu"
         OLD_RPATH "/opt/ros/kinetic/lib:/opt/ros/kinetic/lib/x86_64-linux-gnu:/usr/local/lib/osi3:/usr/local/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/ketisim_inno_bridge_daegu/ketisim_inno_bridge_daegu")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/ketisim_inno_bridge_daegu" TYPE DIRECTORY FILES "/home/keti/ketisim_interface/src/ketisim-inno-bridge-daegu/include/ketisim_inno_bridge_daegu/")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/ketisim_inno_bridge_daegu/launch" TYPE DIRECTORY FILES "/home/keti/ketisim_interface/src/ketisim-inno-bridge-daegu/launch/" REGEX "/\\.svn$" EXCLUDE)
endif()

